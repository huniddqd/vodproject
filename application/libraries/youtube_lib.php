<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Youtube_lib
{
	protected $ci;
	protected $youtube;
	protected $DEVELOPER_KEY = 'AIzaSyDE-l08xvi1MvO2pCuITB1EX2n_fH4wRqk';

	public function __construct()
	{
		require_once APPPATH.'third_party/youtube/src/Google/Client.php';
		require_once APPPATH.'third_party/youtube/src/Google/Service/YouTube.php';
		$this->ci =& get_instance();
		$client = new Google_Client();
		$client->setDeveloperKey($this->DEVELOPER_KEY);

	  // Define an object that will be used to make all API requests.
		$this->youtube = new Google_Service_YouTube($client);
	}

	public function get_youtube()
	{
		return $this->youtube;
	}

	

	

}

/* End of file youtube_lib.php */
/* Location: ./application/libraries/youtube_lib.php */