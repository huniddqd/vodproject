<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($page = 0)
	{
		if($this->session->userdata('loged_in') == TRUE){
			$this->load->view('head_loged');
			$this->load->view('navbar_loged');
		}else {
			$this->load->view('head');
			$this->load->view('navbar');
		}

		$this->load->helper('url');

		$this->load->model('series_model', 'series');
		$this->load->model('video_model', 'video');
		$this->load->model('player_model', 'player');
		$this->load->model('channel_model', 'channel');
		$data['series'] = $this->series->get_closed_series();

		foreach ($data['series'] as $key => $value) {
			$data['series'][$key]['videos'] = $this->video->get_videos_by_series($value['series_id']);
			$data['series'][$key]['players'] = $this->player->get_players_by_series($value['series_id']);
			$data['series'][$key]['channels'] = $this->channel->get_channel_by_id($value['channels_id']);
		}

		$this->paginated_list_view(base_url() . 'index.php/welcome/p/', $data, $page, 3);
		
		
		$this->load->view('panels');
		$this->load->view('footer');
	}

	public function p($page = 0)
	{
		$this->index($page);
	}

	  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */