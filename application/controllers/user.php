<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function login(){

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->model('user_model');
		$this->load->library('recaptcha_lib');

		$this->form_validation->set_rules('login_password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('login_email', 'Email Address', 'required|valid_email');

		if ($this->form_validation->run() == TRUE) 	{
			if($this->user_model->bruteforce($this->input->post('login_email'))){
				echo "To many attempts, you need to wait 15 minutes before next try";
			}elseif($this->user_model->login_user($this->input->post('login_email'),$this->input->post('login_password')) > 0){

				$array = array(
					'user_id' => $this->user_model->get_userid($this->input->post('login_email')),
					'username' => $this->user_model->get_username($this->input->post('login_email')),
					'admin' => $this->user_model->get_privilage($this->input->post('login_email')),
					'email' => $this->input->post('login_email'),
					'loged_in' => TRUE
					);
				$this->session->set_userdata( $array );
			}else {
				echo "Invalid email address or password.";
			}
		}else {
			echo validation_errors();
		}
	}

	public function register(){
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->model('user_model');
		$this->load->library('recaptcha_lib');

		$privatekey = "6Ld34P4SAAAAAAPXBKD1NOlX7BhF30yJMWY1LIBP";
		$secret = "6Ld34P4SAAAAAAPXBKD1NOlX7BhF30yJMWY1LIBP";
		$reCaptcha = new ReCaptcha($secret);
		$resp = null;
		
		
		$this->form_validation->set_rules('reg_username', 'Username', 'required|min_length[4]|is_unique[users.username]');
		$this->form_validation->set_rules('reg_password1', 'Password', 'required|min_length[6]|matches[reg_password2]');
		$this->form_validation->set_rules('reg_password2', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('reg_email', 'Email Address', 'required|valid_email|is_unique[users.email]');

		$resp = $reCaptcha->verifyResponse($this->input->server("REMOTE_ADDR"),$this->input->post("g-recaptcha-response"));
		if ($this->form_validation->run() == TRUE && $resp->success && $resp != null)
		{	
			return $this->user_model->add_user($this->input->post('reg_email'),$this->input->post('reg_username'),$this->input->post('reg_password1'));
		}
		else
		{
			if($resp->success){
				echo validation_errors();
			}else{
				echo "You need to solve the captcha!";
			}			
		}
	}
	public function delete($id)
	{	
		$this->load->model('user_model');
		if($this->session->userdata('user_id') == $id || $this->session->userdata('admin') == TRUE){
			$this->user_model->delete($id);
			if($this->session->userdata('user_id')==$id){
				$this->session->sess_destroy();
				redirect('/', 'refresh');
			}else{
				redirect('/index.php/admin/users', 'refresh');
			}
		}else{
			echo "You don't have privilege to do this.";

		}
	}

	public function log_out()
	{
		$this->session->sess_destroy();
		redirect('/', 'refresh');
	}

	public function username_unique($post_name)
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules($post_name, 'Username', 'is_unique[users.username]');
		if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode(array('valid' => FALSE,));
		}
		else
		{
			echo json_encode(array('valid' => TRUE,));
		}
	}

	public function email_unique($post_name)
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules($post_name, 'Username', 'is_unique[users.email]');
		if ($this->form_validation->run() == FALSE)
		{	
			echo json_encode(array('valid' => FALSE,));
		}
		else
		{
			echo json_encode(array('valid' => TRUE,));
		}
	}

	public function change()
	{
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->model('user_model');
		
		
		$this->form_validation->set_rules('new_username', 'Username', 'min_length[4]|is_unique[users.username]');
		$this->form_validation->set_rules('new_email', 'Email Address', 'valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('old_pass', 'Old Password', 'required|min_length[6]');
		$this->form_validation->set_rules('new_pass', 'New Password', 'min_length[6]');
		$this->form_validation->set_rules('new_pass_re', 'New Password Confirmation', 'min_length[6]|matches[new_pass]');

		if ($this->form_validation->run() == TRUE)
		{	
			if($this->input->post('new_username') == "" && $this->input->post('new_email') == "" && $this->input->post('new_pass') == "" && $this->input->post('delete') == ""){
				echo "There was no changes";
			}else {
				if($this->user_model->login_user($this->session->userdata('email'),$this->input->post('old_pass')) > 0){
					if($this->input->post('new_username') != ""){
						$this->user_model->change($this->input->post('new_username') ,"username");
					}
					if($this->input->post('new_email') != ""){
						$this->user_model->change($this->input->post('new_email') ,"email");
					}
					if($this->input->post('new_pass') != ""){
						$this->user_model->change($this->input->post('new_pass') ,"pass");
					}
					if($this->input->post('delete') == TRUE){
						$this->delete($this->session->userdata('user_id'));
					}
				}
				else{
					echo "The old password is incorrect";
				}
			}
		}
		else
		{
			echo validation_errors();
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */