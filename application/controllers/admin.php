<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('loged_in') == TRUE && $this->session->userdata('admin') == true){
		}else {
			redirect('/', 'refresh');
		}
	}
	
	public function index()
	{
		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('footer');
		
	}

	public function users()
	{
		$this->load->model('user_model');
		$data['users'] = $this->user_model->get_all_users();

		
		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('admin/admin_users_view', $data);
		$this->load->view('footer');
	}

	public function new_videos($channel = '', $token = '')
	{
		$this->load->model('youtube_model', 'youtube');
		$this->load->model('event_model', 'event');
		$this->load->model('series_model', 'series');
		$this->load->model('games_model', 'games');
		$this->load->model('player_model', 'player');
		$this->load->model('video_model', 'video');
		$this->load->model('channel_model', 'channel');

		$this->load->library('youtube_lib');
		
		$data = array();
		$data['channels'] = $this->channel->get_all_channels();
		$data['all_open_series'] = $this->series->get_open_series();
		$data['all_games'] = $this->games->get_all_games();
		$data['all_event'] = $this->event->get_all_event();
		$data['all_player'] = $this->player->get_all_player();

		$q = '';

		if($this->input->post('query')){
			$q = $this->input->post('query');
		}

		

		if($channel != ''){
			$data['new_videos'] = $this->youtube->get_new_videos($this->youtube_lib->get_youtube(), $channel, $token, $q);
			foreach ($data['new_videos'] as $key => $video) {
				if($this->video->exist_by_youtube_id($video['id']['videoId'])){
					$data['new_videos'][$key]=null;
				}
			}
			$data['current_channel'] = $channel;
		}

		if ($this->input->post('youtube_id') && $this->input->post('game') && $this->input->post('series_id')) {
			$new_video = array(
				'series_id' => $this->input->post('series_id'),
				'youtube_id' => $this->input->post('youtube_id'),
				'game' => $this->input->post('game'),
				'title' => $this->input->post('title')
				);
			$this->video->add_video($new_video);

			if($this->input->post('close')){
				$this->series->close($this->input->post('series_id'));
			}
			redirect('/index.php/admin/new_videos/' . $channel, 'refresh');
		}

		
		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('admin/admin_new_videos', $data);
		$this->load->view('footer');
		
	}

	public function videos()
	{
		$data = array();
		$this->load->model('video_model', 'video');
		$this->load->model('series_model', 'series');
		$data['videos'] = $this->video->get_all_videos();

		foreach ($data['videos'] as $key => $video) {
			$data['videos'][$key]['series'] = $this->series->get_by_id($video['series_id']);
		}

		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('admin/admin_videos', $data);
		$this->load->view('footer');
		
	}

	public function channels()
	{
		$this->load->helper('form'); 
		$this->load->model('youtube_model', 'youtube');
		$this->load->model('channel_model', 'channel');
		$data = array();
		$data['channels'] = $this->channel->get_all_channels();

		$data['form_input_attributs'] = array(
			'name'        		=> 'new_channel_id',
			'channels_id'       => 'new_channel_id',
			'value'       		=> 'youtube_id'
			);
		$data['form_attributs'] = array('id' => 'add_channel_form');

		
		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('admin/admin_channels', $data);
		$this->load->view('footer');
		
	}

	public function events()
	{
		$this->load->model('event_model', 'event');
		$this->load->model('youtube_model', 'youtube');
		$this->load->model('channel_model', 'channel');
		$data = array();
		$data['events'] = $this->event->get_all_event();
		$data['channels'] = $this->channel->get_all_channels();

		$new_event_post = $this->input->post('event_name');

		foreach ($data['events'] as $key => $event) {
			$data['events'][$key]['channels'] = $this->event->get_channels($event['events_id']);
		}

		if($new_event_post != false){
			$new_event = array(
				'name' => $new_event_post,
				'name_uri' => url_title($new_event_post)
				);
			$this->event->create_event($new_event);
			redirect('/index.php/admin/events', 'refresh');
		}

		if ($this->input->post('add_channel_id')) {
			$add_channel = array(
				'channels_id' => $this->input->post('add_channel_id'),
				'events_id' => $this->input->post('event_id')
				);
			$this->event->add_channel($add_channel);
			redirect('/index.php/admin/events', 'refresh');
		}



		if ($this->input->post('remove_channel_id')) {
			$remove_channel = array(
				'channels_id' => $this->input->post('remove_channel_id'),
				'events_id' => $this->input->post('event_id')
				);
			$this->event->remove_channel($remove_channel);
			redirect('/index.php/admin/events', 'refresh');
		}

		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('admin/admin_events', $data);
		$this->load->view('footer');
		
	}

	public function series()
	{
		$this->load->helper('url');
		$this->load->model('series_model', 'series');
		$this->load->model('games_model', 'games');
		$this->load->model('event_model', 'event');
		$this->load->model('video_model', 'video');
		$this->load->model('player_model', 'player');
		$this->load->model('youtube_model', 'youtube');
		$this->load->model('channel_model', 'channel');

		$data = array();
		$data['channels'] = $this->channel->get_all_channels();
		$data['all_games'] = $this->games->get_all_games();
		$data['series'] = $this->series->get_all_series();
		$data['events'] = $this->event->get_all_event();
		$data['players'] = $this->player->get_all_player();

		foreach ($data['series'] as $key => $value) {
			$data['series'][$key]['videos'] = $this->video->get_videos_by_series($value['series_id']);
			$data['series'][$key]['players'] = $this->player->get_players_by_series($value['series_id']);
			$data['series'][$key]['channels'] = $this->channel->get_channel_by_id($value['channels_id']);
		}

		if($this->input->post('new_series_name') && $this->input->post('new_series_channel') && $this->input->post('new_series_bo') && $this->input->post('new_series_event') && $this->input->post('new_channels_id') ){
			$new_series = array(
				'name' => $this->input->post('new_series_name'),
				'games_id' => $this->input->post('new_series_channel'),
				'bo' => $this->input->post('new_series_bo'),
				'closed' => false,
				'events_id' => $this->input->post('new_series_event'),
				'channels_id' => $this->input->post('new_channels_id')
				);
			$this->series->create_series($new_series);
			redirect('/index.php/admin/series', 'refresh');
		}

		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('admin/admin_series', $data);
		$this->load->view('footer');
	}
	public function player($value='')
	{
		$this->load->model('player_model', 'player');
		$data = array();
		$data['players'] = $this->player->get_all_player();

		if($this->input->post('new_player_name') && $this->input->post('new_player_picture')){
			$new_player = array(
				'name' => $this->input->post('new_player_name'),
				'picture' => $this->input->post('new_player_picture')
				);
			$this->player->create_player($new_player);
			redirect('/index.php/admin/player', 'refresh');
		}

		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('admin/admin_nav');
		$this->load->view('admin/admin_player', $data);
		$this->load->view('footer');
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
