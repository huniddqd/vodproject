<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Youtube extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('loged_in') != TRUE && $this->session->userdata('admin') != true){
			redirect('/', 'refresh');
		}
	}

	public function delete_channel($channel_id = "")
	{
		$this->load->model('youtube_model', 'youtube');
		$this->load->model('channel_model', 'channel');

		$this->channel->delete_channel($channel_id);
		redirect('/index.php/admin/channels', 'refresh');

	}

	public function add_channel()
	{
		$this->load->model('youtube_model', 'youtube');
		$this->load->model('channel_model', 'channel');
		$this->load->library('youtube_lib');
		$youtube = $this->youtube_lib->get_youtube();

		$channel_id = $this->input->post('new_channel_id', TRUE);
		$this->channel->add_channel($channel_id, $this->youtube->get_channel_by_id($channel_id,$youtube)['items'][0]['snippet']['title']);
		redirect('/index.php/admin/channels', 'refresh');
	}
}

/* End of file youtube.php */
/* Location: ./application/controllers/youtube.php */