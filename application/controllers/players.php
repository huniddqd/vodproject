<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Players extends CI_Controller {
	
	public function __construct()
	{

		parent::__construct();
		if($this->session->userdata('loged_in') != TRUE && $this->session->userdata('admin') != true){
			redirect('/', 'refresh');
		}else{
			$this->load->model('series_model');
		}
	}
	
	public function remove_player($players_id)
	{
		$this->load->model('player_model', 'player');
		$data['players_id'] = $this->input->post('players_id');

		if($data['players_id'] == ''){
			$data['players_id'] = $players_id;
		}

		echo $data['players_id'];

		$this->player->remove_player($data);

		redirect('index.php/admin/player');
	}
	
}

/* End of file players.php */
	/* Location: ./application/controllers/players.php */