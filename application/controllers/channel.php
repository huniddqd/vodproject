<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Channel extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function c($channel_name_uri, $page = 0)
	{
		if($this->session->userdata('loged_in') == TRUE){
			$this->load->view('head_loged');
			$this->load->view('navbar_loged');
		}else {
			$this->load->view('head');
			$this->load->view('navbar');
		}

		$this->load->model('series_model', 'series');
		$this->load->model('video_model', 'video');
		$this->load->model('player_model', 'player');
		$this->load->model('channel_model', 'channel');
		$this->load->model('event_model', 'event');

		$channel = $this->channel->get_id_by_uri($channel_name_uri);

		$data['series'] = $this->series->get_series_by_channel($channel[0]['channels_id']);

		foreach ($data['series'] as $key => $value) {
			$data['series'][$key]['videos'] = $this->video->get_videos_by_series($value['series_id']);
			$data['series'][$key]['players'] = $this->player->get_players_by_series($value['series_id']);
			$data['series'][$key]['channels'] = $this->channel->get_channel_by_id($value['channels_id']);
		}

		$this->paginated_list_view(base_url() . 'index.php/channel/c/' . $channel_name_uri . '/', $data, $page, 4);
		$this->load->view('panels');
		$this->load->view('footer');
	}

	public function p($page = 0)
	{
		$this->c($page);
	}

}

/* End of file event.php */
/* Location: ./application/controllers/event.php */