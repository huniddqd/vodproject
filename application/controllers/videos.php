<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('loged_in') == TRUE && $this->session->userdata('admin') == true){
		}else {
			redirect('/', 'refresh');
		}
	}

	public function add_videos()
	{
		$this->load->model('video_model', 'video');

		$new_video =array();
		$i=0;
		foreach ($this->input->post()['game'] as $key => $value) {
			if($value>=0){
				$new_video['title'] = $this->input->post('title')[$key];
				$new_video['youtube_id'] = $this->input->post('youtube_id')[$key];
				$new_video['game'] = $this->input->post('game')[$key];
				$new_video['series_id'] = $this->input->post('series_id')[$key];
				$this->video->add_video($new_video);
			}
		}
		redirect('/index.php/admin/new_videos/' . $this->input->post('channel'), 'refresh');
	}

	public function add_timestamp()
	{
		$this->load->model('video_model', 'video');

		$new_video =array();
		$i=0;
		foreach ($this->input->post('timestampstart') as $key => $value) {
			if($value!=0){
				$new_video['title'] = $this->input->post('title');
				$new_video['youtube_id'] = $value . "-" . $this->input->post('timestampend')[$key];
				$new_video['game'] = $this->input->post('game')[$key];
				$new_video['series_id'] = $this->input->post('series_id');
				$this->video->add_video($new_video);
			}
		}


		redirect('/index.php/admin/series/#' . $this->input->post('series_id') , 'refresh');
	}

}

/* End of file videos.php */
/* Location: ./application/controllers/videos.php */