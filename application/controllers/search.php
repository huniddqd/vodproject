<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function q()
	{
		$this->load->model('search_model', 'search');
		$query = $this->input->get('query');
		if($query != ''){
			$data['player'] = $this->search->player($query);
			$data['event'] = $this->search->event($query);
			$data['channel'] = $this->search->channel($query);
			$data['series'] = $this->search->series($query);

			print_r(json_encode($data));
		}
	}
}

/* End of file search.php */
/* Location: ./application/controllers/search.php */