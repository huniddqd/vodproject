<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('loged_in') != TRUE && $this->session->userdata('admin') != true){
			redirect('/', 'refresh');
		}
	}

	public function index()
	{
		$this->load->view('head_loged');
		$this->load->view('navbar_loged');
		$this->load->view('settings');
		$this->load->view('panels');
		$this->load->view('footer');
	}
}

/* End of file settings.php */
/* Location: ./application/controllers/settings.php */