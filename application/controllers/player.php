<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	

	public function s($series = "", $video = "1")
	{

		$this->load->model('series_model', 'series');
		$this->load->model('series_model', 'series');
		$this->load->model('video_model', 'video');
		$this->load->model('player_model', 'player');
		$this->load->model('channel_model', 'channel');
		$this->load->model('event_model', 'event');

		$data['series'] = $this->series->get_by_id($series);
		$data['video'] = $video;

		foreach ($data['series'] as $key => $value) {
			$data['series'][$key]['videos'] = $this->video->get_videos_by_series($value['series_id']);
			$data['series'][$key]['players'] = $this->player->get_players_by_series($value['series_id']);
			$data['series'][$key]['channels'] = $this->channel->get_channel_by_id($value['channels_id']);
			$data['series'][$key]['event'] = $this->event->get_by_id($value['events_id']);
		}

		if($series == ""){
			redirect('/', 'refresh');
		}

		$this->load->view('head');
		if($this->session->userdata('loged_in') == TRUE){
			$this->load->view('navbar_loged');
		}else {
			$this->load->view('navbar');
		}
		$this->load->view('player', $data);
		$this->load->view('panels');
		$this->load->view('footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */