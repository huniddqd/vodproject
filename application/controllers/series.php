<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Series extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('loged_in') != TRUE && $this->session->userdata('admin') != true){
			redirect('/', 'refresh');
		}else{
			$this->load->model('series_model');
		}
	}

	public function get_open_series()
	{
		return $this->series_model->get_open_series();
	}

	public function create_series()
	{
		$this->load->model('event_model', 'event');
		$this->load->model('player_model', 'player');
		$this->load->model('games_model', 'games');
		$data = array(
			'name' => $this->input->post('series_name'),
			'player1' => $this->input->post('player1'),
			'player2' => $this->input->post('player2'),
			'game' => $this->input->post('game'),
			'bo' => $this->input->post('bo'),
			'closed' => $this->input->post('closed'),
			'event' => $this->input->post('event'),
			'channel' => $this->input->post('channel'),
			'new_player1' => $this->input->post('new_player1'),
			'new_player2' => $this->input->post('new_player2'),
			'new_event' => $this->input->post('new_event')
			);

		if($data['new_event']!=''){
			$event = array(
				'name' => $data['new_event'],
				'game' => $data['game']
				);
			$this->event->create_event($event);
			$data['event'] = $this->event->get_id($this->input->post('new_event'))[0]['id'];
		}else{
			$data['event'] = $this->event->get_id($this->input->post('event'))[0]['id'];
		}
		if($data['new_player1']!=''){
			$player = array(
				'name' => $data['new_player1'],
				'picture' => 'default_player.png'
				);
			$this->player->create_player($player);
			$data['player1'] = $this->player->get_id($this->input->post('new_player1'))[0]['id'];
		}

		if($data['new_player2']!=''){
			$player = array(
				'name' => $data['new_player2'],
				'picture' => 'default_player.png'
				);
			$this->player->create_player($player);
			$data['player2'] = $this->player->get_id($this->input->post('new_player2'))[0]['id'];
		}

		array_splice($data, 8);

		$this->series_model->create_series($data);
	}

	public function add_video()
	{
		$this->load->model('video_model', 'video');
		$this->load->model('series_model', 'series');
		$data = array(
			'series_id' => $this->input->post('series_id'),
			'match' => $this->input->post('match'),
			'youtube_id' => $this->input->post('youtube_id'),
			'closed' => $this->input->post('closed_add_video_form')

			);

		if($data['closed'] == true){
			$this->series->close($data['series_id']);
		}
		array_splice($data, 3);

		

		$this->video->add_video($data);
	}

	public function add_player()
	{
		$this->load->model('player_model', 'player');
		$this->load->model('series_model', 'series');

		$data['players_id'] = $this->input->post('players_id');
		$data['series_id'] = $this->input->post('series_id');
		$this->series->add_player($data);

		redirect('index.php/admin/series#' . $data['series_id']);

	}

	public function remove_player()
	{
		$this->load->model('player_model', 'player');
		$this->load->model('series_model', 'series');

		$data['players_id'] = $this->input->post('players_id');
		$data['series_id'] = $this->input->post('series_id');

		$this->series->remove_player($data);

		redirect('index.php/admin/series/#' . $data['series_id']);

	}

	public function close_series()
	{
		$this->load->model('series_model', 'series');
		$series_id = $this->input->post('series_id');
		$close = $this->input->post('close');

		if($series_id!= '' && $close != ''){
			$this->series->close($series_id);
		}
		redirect('index.php/admin/series');
	}
}

/* End of file series.php */
/* Location: ./application/controllers/series.php */