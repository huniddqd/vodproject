<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function paginated_list_view($base_url ,$data, $page, $uri_segment)
	{
		$this->load->library('pagination');
		
		$per_page = 25;


		$config['base_url'] = $base_url;
		$config['total_rows'] = count($data['series']);
		$config['per_page'] = $per_page;
		$config['uri_segment'] = $uri_segment;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav>';
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li class="pull-right">';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li class="pull-right">';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$data['series'] = array_slice($data['series'], $page, $per_page);

		$this->pagination->initialize($config);
		
		$this->load->view('list', $data);
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */