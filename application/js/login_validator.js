$(function(){
    $("[data-hide]").on("click", function(){
        $("." + $(this).attr("data-hide")).parent().parent().hide();
    });
});

$( "#register" ).click(function() {
	$( ".login" ).toggleClass( "hidden" );
	$( ".register" ).toggleClass( "hidden" );
});

$( "#login" ).click(function() {
	$( ".login" ).toggleClass( "hidden" );
	$( ".register" ).toggleClass( "hidden" );
});

var onloadCallback = function() {
	grecaptcha.render('reg_captcha', {
		'sitekey' : '6Ld34P4SAAAAAJ26B9Pe_lQuw5-CFAK4iULtmcSp'
	});
};

$(document).ready(function() {
	$('#loginForm').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			login_email: {
				validators: {
					notEmpty: {
						message: 'The email address is required'
					},
					emailAddress: {
						message: 'The value is not a valid email address'
					},
					regexp: {
						regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message: 'The value is not a valid email address'
					}
				}
			},
			login_password: {
				validators: {
					notEmpty: {
						message: 'The password is required'
					},
					stringLength: {
						min: 6,
						message: 'The password must be at least 6 characters'
					}
				}
			}
		}
	}).on('err.validator.fv', function(e, data) {
            // $(e.target)    --> The field element
            // data.fv        --> The FormValidation instance
            // data.field     --> The field name
            // data.element   --> The field element
            // data.validator --> The current validator name

            data.element
            .data('fv.messages')
                // Hide all the messages
                .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                // Show only message associated with current validator
                .filter('[data-fv-validator="' + data.validator + '"]').show();
            }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            
            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result == ""){                    
                    $("#login_succsesful").show();
                    $('#loginModal').modal('toggle');
                    location.reload();
                }else{
                    $("#login_alert_massage").html(result);
                    $("#login_alert").show();
                }
            }).fail(function(data){
                console.log(data);
            });
        });
        });

$(document).ready(function() {
	$('#registerForm').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: { 
            reg_username: {
                validators: {
                    notEmpty: {
                        message: 'The username is required'
                    },
                    stringLength: {
                        min: 4,
                        message: 'The username must be at least 4 characters'
                    },
                    remote: {
                        message: 'The username is not available',
                        url: '/vodproject/index.php/user/username_unique/reg_username',
                        type: 'POST'
                    }
                }
            },
            reg_email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The value is not a valid email address'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address'
                    }
                }
            },
            reg_password1: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must be at least 6 characters'
                    }
                }
            },
            reg_password2: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    identical: {
                        field: 'reg_password1',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    }).on('err.validator.fv', function(e, data) {
            // $(e.target)    --> The field element
            // data.fv        --> The FormValidation instance
            // data.field     --> The field name
            // data.element   --> The field element
            // data.validator --> The current validator name

            data.element
            .data('fv.messages')
                // Hide all the messages
                .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                // Show only message associated with current validator
                .filter('[data-fv-validator="' + data.validator + '"]').show();
            }).on('err.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.fv      --> The FormValidation instance
            // data.field   --> The field name
            // data.element --> The field element

            data.fv.disableSubmitButtons(false);
        })
            .on('success.field.fv', function(e, data) {
            // e, data parameters are the same as in error.field.bv event handler
            // Despite that the field is valid, by default, the submit button will be disabled if all the following conditions meet
            // - The submit button is clicked
            // - The form is invalid
            data.fv.disableSubmitButtons(false);
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);
            
            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result == ""){
                    $("#reg_succsesful").show();
                    $('#loginModal').modal('toggle');
                }else{
                    $("#reg_alert_massage").html(result);
                    $("#reg_alert").show();
                }
            });
        });
    });

$(document).ready(function() {
    $('#change_form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: { 
            new_email: {
                validators: {
                    emailAddress: {
                        message: 'The value is not a valid email address'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'The value is not a valid email address'
                    },
                    remote: {
                        message: 'This Email Address Already in Use',
                        url: '/vodproject/index.php/user/email_unique/new_email',
                        type: 'POST'
                    }
                }
            },
            new_username: {
                validators: {
                    stringLength: {
                        min: 4,
                        message: 'The username must be at least 4 characters'
                    },
                    remote: {
                        message: 'The username is not available',
                        url: '/vodproject/index.php/user/username_unique/new_username',
                        type: 'POST'
                    }
                }
            },
            old_pass: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must be at least 6 characters'
                    }
                }
            },
            new_pass: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'The password must be at least 6 characters'
                    }
                }
            },
            new_pass_re: {
                validators: {
                    identical: {
                        field: 'new_pass',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            delete: {
                validators: {
                }
            }
        }
    }).on('err.validator.fv', function(e, data) {
        // $(e.target)    --> The field element
        // data.fv        --> The FormValidation instance
        // data.field     --> The field name
        // data.element   --> The field element
        // data.validator --> The current validator name

        data.element
        .data('fv.messages')
            // Hide all the messages
            .find('.help-block[data-fv-for="' + data.field + '"]').hide()
            // Show only message associated with current validator
            .filter('[data-fv-validator="' + data.validator + '"]').show();
        }).on('err.field.fv', function(e, data) {
        // $(e.target)  --> The field element
        // data.fv      --> The FormValidation instance
        // data.field   --> The field name
        // data.element --> The field element

        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            
            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result == ""){
                    location.reload();
                    $("#change_successful").show();
                }else{
                    $("#change_alert_massage").html(result);
                    $("#change_alert").show();
                }
            });
        });
    });