$(document).ready(function($) {
	$("#search").keyup(function(event) {
		var $form = $(event.currentTarget);
		$.get($form.attr('action'), $form.serialize(), function(result) {
			var $player = '';
			var $series = '';
			var $event = '';
			var $channel = '';
			$.each(result.player, function(index, value) {
				$player += "<div class=\"result_found\">" + value.name + "</div>";
			});
			$.each(result.series, function(index, value) {
				$series += "<div class=\"result_found\">" + value.name + "</div>";
			});
			$.each(result.event, function(index, value) {
				$event += "<div class=\"result_found\">" + value.name + "</div>";
			});
			$.each(result.channel, function(index, value) {
				$channel += "<div class=\"result_found\">" + value.name + "</div>";
			});
			$("#result").html("");
			if($player != ''){
				$("#result").append("<div class=\"result_type\">Player</div>" + $player + "</div>");
				$("#result").show();
			}
			if($series != ''){
				$("#result").append("<div class=\"result_type\">Series</div>" + $series + "</div>");
				$("#result").show();
			}
			if($event != ''){
				$("#result").append("<div class=\"result_type\">Event</div>" + $event + "</div>");
				$("#result").show();
			}
			if($channel != ''){
				$("#result").append("<div class=\"result_type\">Channel</div>" + $channel + "</div>");
				$("#result").show();
			}
			if($player == '' && $series == '' && $event == '' && $channel == ''){
				$("#result").text("");
				$("#result").hide();
			}
			
		}, "json");
	});

	$("#search-div").focusout(function(event) {
		if($("#result").text()!=""){
			$("#result").hide();
		}
	});
	$("#search-div").focusin(function(event) {
		if($("#result").text()!=""){
			$("#result").show();
		}
	});
});
