<div class="col-xs-12 col-lg-8 pull-left">
  <h1>Recent Vods <small>all games</small></h1>
  <ul class="list-unstyled gamelist">
    <?php 
    foreach ($series as $value) {
      echo "<li>\n<i class=\"fa fa-youtube-play text-danger\"></i>\n<img src=\"" . base_url() . "application/img/" . $value['icon'] . "\" alt=\"" . $value['game'] . "\"/>\n";
      echo "<a href=\"" . base_url() . "index.php/player/s/" . $value['series_id'] . "\">";
      $players_count = count($value['players']);
      foreach ($value['players'] as $key => $player) {
        if($key == $players_count/2){
          echo " vs ";
        }
        echo $player['name'] . " ";
      }
      echo "(Best of " . $value['bo'] .")</a><span class=\"hidden-xs\"> - <a href=\"" . base_url() . "index.php/event/e/" . url_title($value['events_name']) . "\">" . $value['events_name'] . "</a> / cast by: <a href=\"" . base_url() . "index.php/channel/c/" . url_title($value['channels'][0]['name']) . "\">" . $value['channels'][0]['name'] . "</a></span>\n ";
      echo "</li>\n";
    }
    ?>
  </ul>
 <?php echo $this->pagination->create_links();?>
</div>
