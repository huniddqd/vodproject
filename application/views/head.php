<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Esport vods</title>

	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>application/css/custom.css" rel="stylesheet">
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<!-- Bootstrap -->
	<link href="<?php echo base_url(); ?>application/third_party/css/bootstrap.min.css" rel="stylesheet">

	<!-- FormValidation plugin and the class supports validating Bootstrap form -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>application/third_party/formvalidation/dist/css/formValidation.min.css">
	<script src="<?php echo base_url(); ?>application/third_party/formvalidation/dist/js/formValidation.min.js"></script>
	
	<script src="<?php echo base_url(); ?>application/third_party/formvalidation/dist/js/framework/bootstrap.min.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
  </head>
  <body>