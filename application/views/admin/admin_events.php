<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <td><strong>event id</strong></td>
            <td><strong>name</strong></td>
            <td><strong>channel(s)</strong></td>
            <td><strong>action</strong></td>
        </tr>
    </thead>
    <tbody>
        <?php if(isset($events)){
            foreach ($events as $event) { ?>
            <tr>
                <td><?php echo $event['events_id'];?></td>
                <td><?php echo $event['name'];?></td>
                
                <td>
                    <?php
                    if(isset($event['channels'])){
                        foreach ($event['channels'] as $channel) {
                            echo $channel['name'] . "; ";
                        }
                    }
                    ;?>

                </td>
                <td> 
                    <form class = "form-inline" action="<?php echo base_url();?>index.php/admin/events" method="post">
                        <select class="form-control" name="add_channel_id">
                            <?php
                            foreach ($channels as $value) {
                                echo "<option value = \"" . $value['channels_id'] . "\">" . $value['name'] . "</option>";
                            }
                            ?>
                        </select>
                        <input type="hidden" name="event_id" value="<?php echo $event['events_id'];?>">
                        <button type="submit" class="btn btn-primary">add channel</button>
                    </form>

                    <form class = "form-inline" action="<?php echo base_url();?>index.php/admin/events" method="post">
                        <select class="form-control" name="remove_channel_id">
                            <?php
                                foreach ($event['channels'] as $channel) {
                                    echo "<option value = \"" . $channel['channels_id'] . "\">" . $channel['name'] . "</option>";
                                }
                            ?>
                        </select>
                        <input type="hidden" name="event_id" value="<?php echo $event['events_id'];?>">
                        <button type="submit" class="btn btn-primary">remove channel</button>
                    </form></td>
                </tr>
                <?php }
            }?>
            <tr>
                <form action="<?php echo base_url();?>index.php/admin/events" method="post">
                    <td></td>
                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control" id="event_name" name="event_name" placeholder="event name">
                        </div>
                    </td>
                    <td></td>
                    <td>
                        <button type="submit" class="btn btn-default"><i class="fa fa-plus"></i></button></button>
                    </td>
                </form>
            </tr>
        </tbody>
    </table>