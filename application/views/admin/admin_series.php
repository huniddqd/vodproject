<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <td><strong>series id</strong></td>
            <td><strong>name</strong></td>
            <td><strong>game</strong></td>
            <td><strong>bo</strong></td>
            <td><strong>closed</strong></td>
            <td><strong>event</strong></td>
            <td><strong>players</strong></td>
            <td><strong>channel</strong></td>
            <td><strong>videos</strong></td>
            <td><strong>action</strong></td>
        </tr>
    </thead>
    <tbody>

        <?php
        foreach ($series as $value) {

            echo "<tr id=\"" . $value['series_id'] . "\">
            <td>" . $value['series_id']. "</td>
            <td>" . $value['name']. "</td>
            <td>" . $value['game']. "</td>
            <td>" . $value['bo']. "</td>
            <td>" . $value['closed']. "</td>
            <td>" . $value['events_name']. "</td>
            <td>";
                foreach ($value['players'] as $temp) {
                    echo "<form action=\"" . base_url() ."index.php/series/remove_player\" method=\"post\">
                    <input name=\"series_id\"  type=\"hidden\" value=\"" . $value['series_id'] . "\">
                    <input name=\"players_id\" type=\"hidden\" value=\"" . $temp['players_id'] . "\">";
                    echo $temp['name'];
                    echo "<button class =\"btn-xs btn\" type=\"submit\"><i class=\"fa fa-trash-o\"></i></button><br /></form>";
                }

                echo "<form class=\"form-inline\" action=\"" . base_url() . "index.php/series/add_player\" method=\"post\">\n
                <select class=\"form-control\" name=\"players_id\">\n";
                    foreach ($players as $player) {
                        echo "<option value=\"" . $player['players_id'] . "\">" . $player['name'] . "</option>\n";
                    }

                    echo    "
                </select>\n
                <input type=\"hidden\" name=\"series_id\" value=\"" . $value['series_id'] . "\">
                <button class=\"btn btn-primary btn-xs\" type=\"submit\">Add Player</button>
            </form>
        </td>
        <td>" . $value['channels'][0]['name'] . "</td>
        <td><ul class =\"list-unstyled\">";
            foreach ($value['videos'] as $video) {
                echo "<li><a href=\"http://youtube.com/embed/" . $video['youtube_id'] . "\" target=\"_blank\">" . $video['title'] . "</a></li>";
                if($video['game']==0){
                    
                    echo '<form class="form-inline" action="' . base_url() . '/index.php/videos/add_timestamp" method="post">';
                    for ($i=1; $i <= $value['bo']; $i++) { 
                        echo'
                        <label>game ' . $i .'</label>
                        <div class="row">
                        <div class="col-xs-4">
                        <input type="number" name="timestampstart[]" class="form-control input-sm" placeholder="timestampstart" value="0"/>
                        </div>
                        <div class="col-xs-4">
                        <input type="number" name="timestampend[]" class="form-control input-sm" placeholder="timestampend" value="0"/><br />
                        </div>
                        </div>
                        <input type="hidden" name="series_id" value="' . $value['series_id'] . '"/>
                        <input type="hidden" name="game[]" value="' . $i . '"/>
                        <input type="hidden" name="title" value="' . $value['videos'][0]['title'] . '"/>
                        ';
                        
                    }
                    echo '<button type="submit" class="btn btn-primary"> add </button>';
                    echo '</form>';
                }
            }
            echo" </ul></td>
            <td>
                <form action=\"" . base_url() . "/index.php/series/close_series\" method=\"post\">
                    <label for=\"close\">close series:</label>
                    <input type=\"checkbox\" name=\"close\" />
                    <input type=\"hidden\" class=\"form-control\" name=\"series_id\" value=\"" . $value['series_id'] . "\" />
                    <button type=\"submit\" class=\"btn btn-primary\">submit</button>
                </form>
            </td>
        </tr>";

    }
    ?>


    <tr>
        <td></td>
        <form action="<?php echo base_url();?>index.php/admin/series" method="post"">
            <td><input class="form-control" type="text" name="new_series_name"></td>
            <td>
                <select class="form-control" name="new_series_channel">
                    <?php
                    foreach ($all_games as $value) {
                        echo "<option value = \"" . $value['games_id'] . "\">" . $value['game'] . "</option>";
                    }
                    ?>
                </select>
            </td>
            <td><input class="form-control" type="number" name="new_series_bo" value="0" step="1"></td>
            <td>
                <select class="form-control" name="new_channels_id">
                    <?php
                    foreach ($channels as $value) {
                        echo "<option value = \"" . $value['channels_id'] . "\">" . $value['name'] . "</option>";
                    }
                    ?>
                </select></td>
                <td>
                    <select class="form-control" name="new_series_event">
                        <?php
                        foreach ($events as $value) {
                            echo "<option value = \"" . $value['events_id'] . "\">" . $value['name'] . "</option>";
                        }
                        ?>
                    </select>
                </td>
                <td><button type="submit" class="btn btn-primary">add series</button></td>
            </form>
        </tr>
    </tbody>
</table>