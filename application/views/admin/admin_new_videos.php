<div class="dropdown">
	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
		Channels
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
		<?php foreach ($channels as $channel) { ?>
		<li><a role="menuitem" tabindex="-1" href="<?php echo base_url();?>index.php/admin/new_videos/<?php echo $channel['youtube_channel_id'];?>"><?php echo $channel['name'];?></a></li>
		<?php } ?>
	</ul>
</div>

<?php if (isset($new_videos)){ ?>
<form class="form-inline" action="<?php echo base_url();?>/index.php/admin/new_videos/<?php echo $current_channel;?>" method="post">
	<input class="form-control" type="text" name = "query">
	<button type="submit" class="btn btn-primary">Search</button>
</form>
<form class="form-inline" action="<?php echo base_url();?>/index.php/videos/add_videos" method="post">
	<table class="table table-hover table-striped table-responsive">
		<thead>
			<tr>
				<td><strong>video_id</strong></td>
				<td><strong>title</strong></td>
				<td><strong>channel_name</strong></td>
				<td><strong>action</strong></td>
			</tr>
		</thead>
		<tbody>
			
			<?php foreach ($new_videos as $key => $data) {
				if($data['id']['videoId'] != null){
					?>
					<tr>
						<td><strong><a href=https://www.youtube.com/watch?v=<?php echo $data['id']['videoId'];?>><?php echo $data['id']['videoId'];?></a></strong></td>
						<td><strong><?php echo $data['snippet']['title'];?></strong></td>
						<td><strong><?php echo $data['snippet']['channelTitle']?></strong></td>
						<td>
							
							<input type="hidden" name="title[]" value="<?php echo $data['snippet']['title'];?>">
							<input type="hidden" name="youtube_id[]" value="<?php echo $data['id']['videoId'];?>">
							<input class="form-control" type="number" value="-1" step="1" name="game[]">
							<select class="form-control" name="series_id[]">
								<?php
								foreach ($all_open_series as $value) {
									echo "<option value = \"" . $value['series_id'] . "\">" . $value['name'] . "</option>";
								}
								?>
							</select>
						</td>
					</tr>
					<?php }}?>

				</form>
			</tbody>
		</table>
		<input type="hidden" name="channel" />
		<button type="submit" class="btn btn-primary">add video</button>
		<?php if($new_videos['prevPageToken'] != null){ ?>
		<a href="<?php echo base_url();?>index.php/admin/new_videos/<?php echo $current_channel;?>/<?php echo $new_videos['prevPageToken'];?>">prev</a>
		<?php } 
		if($new_videos['nextPageToken'] != null){?>
		<a class ="pull-right" href="<?php echo base_url();?>index.php/admin/new_videos/<?php echo $current_channel;?>/<?php echo $new_videos['nextPageToken'];?>">next</a> 

		<?php }
	}?>