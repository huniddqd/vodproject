<table class="table table-hover table-striped table-responsive">
    <thead>
        <tr>
            <td><strong>channel id</strong></td>
            <td><strong>name</strong></td>
            <td><strong>youtube id</strong></td>
            <td><strong>action</strong></td>
        </tr>
    </thead>
    <tbody>
        <?php if(isset($channels)){
          foreach ($channels as $channel) { ?>
          <tr>
            <td><?php echo $channel['channels_id'];?></td>
            <td><?php echo $channel['name'];?></td>
            <td><?php echo $channel['youtube_channel_id'];?></td>
            <td>
                <a href ="<?php echo base_url();?>index.php/youtube/delete_channel/<?php echo $channel['channels_id'];?>"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
        <?php }
    }?>
    <tr>
        <?php echo form_open("index.php/youtube/add_channel/", $form_attributs);?>
        <td>
        </td>
        <td>
        </td>
        <td>
            <?php echo form_input($form_input_attributs);?>
        </td>
        <td>
            <?php echo form_submit('add_channel', 'Add');?>
        </td>
    </tr>
</tbody>
</table>