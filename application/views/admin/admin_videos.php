<table class= "table table-hover table-striped table-responsive">
	<thead>
		<tr>
			<td>id</td>
			<td>series</td>
			<td>youtube id</td>
			<td>game</td>
			<td>title</td>
			<td>actions</td>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($videos as $video) {
			echo "
				<tr>
					<td>" . $video['videos_id'] . "</td>
					<td>" . $video['series'][0]['name'] . "</td>
					<td>" . $video['youtube_id'] . "</td>
					<td>" . $video['game'] . "</td>
					<td>" . $video['title'] . "</td>
					<td></td>
				</tr>";
		}
		?>
	</tbody>
</table>