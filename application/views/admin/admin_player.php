<table class="table table-hover table-striped table-responsive">
	<thead>
		<tr>
			<td><strong>player id</strong></td>
			<td><strong>name</strong></td>
			<td><strong>picture</strong></td>
			<td><strong>action</strong></td>
		</tr>
	</thead>
	<tbody>
		<?php if(isset($players)){
			foreach ($players as $player) { ?>
			<tr>
				<td><?php echo $player['players_id'];?></td>
				<td><?php echo $player['name'];?></td>
				<td><?php echo $player['picture'];?></td>
				<td><a href="<?php echo base_url()?>index.php/players/remove_player/<?php echo $player['players_id'];?>"><i class="fa fa-trash-o"></i></a></td>
			</tr>
			<?php }
		}?>
		<tr>
			<form action="<?php echo base_url();?>/index.php/admin/player" method="post">
				<td></td>
				<td>
					<input class="form-control" type="text" name="new_player_name">
				</td>
				<td>
				<input class="form-control" type="text" name="new_player_picture" value="default.jpg">
				</td>
				<td>
					<button type="submit" class="btn btn-primary">Submit</button>
				</td>
			</form>
		</tr>
	</tbody>
</table>