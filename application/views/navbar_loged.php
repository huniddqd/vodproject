<div class="row alert_row" id="change_alert">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-hide="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <span id="change_alert_massage"></span>
    </div>
  </div>
  <div class="col-md-4"></div>
</div>
<div class="row alert_row" id="change_successful">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="alert alert-success alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-hide="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      The changes have been saved.
    </div>
  </div>
  <div class="col-md-4"></div>
</div>

<nav class="navbar navbar-default" role="navigation">
  <div class="container">
   <!-- Brand and toggle get grouped for better mobile display -->
   <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
     <span class="sr-only">Toggle navigation</span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
   </button>
   <a class="navbar-brand" href="<?php echo base_url(); ?>">Esport Vods</a>
 </div>


 <!-- Collect the nav links, forms, and other content for toggling -->
 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav">
   <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Games <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
     <li><a href="#">All</a></li>
     <li><a href="#">Counter Strike: Global offensive</a></li>
     <li><a href="#">Dota 2</a></li>
     <li><a href="#">Hearthstone</a></li>
     <li><a href="#">Heroes of the Storm</a></li>
     <li><a href="#">League of Legends</a></li>
     <li><a href="#">Smite</a></li>
     <li><a href="#">Starcraft II</a></li>
     <li><a href="#">World of Tanks</a></li>
   </ul>
 </li>
 <li><a href="#" class="visible-sm">Search</a></li>

</ul>
<div id="search-div">
  <form class="navbar-form navbar-left hidden-sm" role="search" id="search" action="<?php echo base_url();?>index.php/search/q" method="get">
   <div class="form-group">
    <div class="input-group">
    <div id="result"></div>
     <input type="text" id="search-input" name="query" class="form-control" autocomplete="off">
     <span class="input-group-btn">
      <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
    </span>
  </div><!-- /input-group -->
</div>
</form>
</div>
<ul class="nav navbar-nav navbar-right">
 <li><a href="<?php echo base_url();?>index.php/settings">Settings</a></li>
 <?php if($this->session->userdata('admin')== true) echo "<li><a href=\"" . base_url() . "index.php/admin\">Admin</a></li>";?>
 <li><a href="<?php echo base_url();?>index.php/user/log_out">Log out</a></li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<div class="container content">