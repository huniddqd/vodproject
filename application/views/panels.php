  		<div class="row">
  			<div class="col-xs-12 col-lg-4 pull-right">
  				<div class="panel panel-default">
  					<div class="panel-heading">
  						<h3 class="panel-title">Top rated</h3>
  					</div>
  					<div class="panel-body">
  						<ul class="list-unstyled gamelist">
  							<li><i class="fa fa-youtube-play text-danger"></i> <a href="<?php echo base_url();?>index.php/player">[TvT] Polt vs ST_Bomber (BO5 in 1 Video)</a> <span class="hidden-xs">/ cast by: Rotterdam & Nathanias</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i> [ZvT] HyuN vs Heart (BO5 in 1 Video) <span class="hidden-xs">/ cast by: Rotterdam & Nathanias</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i> [ZvT] TRUE vs fantasy (Best of 3) <span class="hidden-xs">/ cast by: Rifkin & ZombieGrub</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i>  Korea vs Norway (Best of 9 - All in 1 video) <span class="hidden-xs">/ cast by: Khaldor</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i> [PvT] CPU vs ArotaX (Best of 3) <span class="hidden-xs">/ cast by: Madals</span><br /></li>
  						</ul>
  					</div>
  				</div>
  				<div class="panel panel-default">
  					<div class="panel-heading">
  						<h3 class="panel-title">Most watched</h3>
  					</div>
  					<div class="panel-body">
  						<ul class="list-unstyled gamelist">
  							<li><i class="fa fa-youtube-play text-danger"></i> [ZvT] Scarlett vs Bunny (Best of 7) <span class="hidden-xs">- The Foreign Hope - Finals / cast by: Madals & Rotterdam</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i> [ZvP] NesTea vs Crank (1 Game) <span class="hidden-xs">- Starcraft Ladder - Battle.net VOD / cast by: Rifkin</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i> [PvZ] Grubby vs Targa (1 Game) <span class="hidden-xs">- Starcraft Ladder - Battle.net VOD / cast by: Rifkin</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i> [PvP] Crank vs Sos (1 Game) <span class="hidden-xs">- Starcraft Ladder - Battle.net VOD / cast by: Rifkin</span><br /></li>
  							<li><i class="fa fa-youtube-play text-danger"></i> [ZvZ] Tefel vs Targa (Best of 5) <span class="hidden-xs">- Go4SC2 Cup - Finals / cast by: Jorosar</span><br /></li>
  						</ul>
  					</div>
  				</div>
  				<div class="panel panel-default visible-lg">
  					<div class="panel-heading">
  						<h3 class="panel-title">Streams</h3>
  					</div>
  					<div class="panel-body">
  						<p class="text-info"><strong class="text-success">Online:</strong> TLO, ForGG, MorroW</p>
  						<p class="text-muted"><strong class="text-danger">Offline:</strong> Jaedong, iNcontroL, HuK, Tyler, CatZ, WhiteRa, Grubby, Destiny, Spanishiwa, PainUser, Minigun, qxc, Dimaga, DeMuslim, LiquidHero, Naniwa, MarineKing, Dragon, DongRaeGu, ToD, Snute, ViBE, Kas, JYP, TOP, KawaiiRice</p>
  					</div>
  				</div>
  			</div>