<div class="col-xs-12 col-lg-8 pull-left">
	<h4><?php
		$players_count = count($series[0]['players']);
		foreach ($series[0]['players'] as $key => $player) {
			if($key == $players_count/2){
				echo " vs ";
			}
			echo $player['name'] . " ";
		} 
		?>
		<small><?php echo "(Best of " . $series[0]['bo'] .")"; ?></small></h4>
		<p>Rate this Series: <span class="text-success">11 <i class="fa fa-thumbs-up"></i></span> <span class="text-danger">5 <i class="fa fa-thumbs-down"></i></span><span class="pull-right">
			<?php
			for ($i=1; $i <= $series[0]['bo']; $i++) { 
				echo "<a href=\"" . base_url() . "index.php/player/s/" . $series[0]['series_id'] . "/$i\">game " . $i ."</a> ";
			}
			?>
		</span></p>
		<!-- 16:9 aspect ratio -->
		<br />
		
		<?php

		if(isset($series[0]['videos'][$video-1]['youtube_id']) && $series[0]['videos'][0]['game']!=0){
			echo '<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="//www.youtube.com/embed/' . $series[0]["videos"][$video-1]["youtube_id"] . '?modestbranding=1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>';
	}elseif ($series[0]['videos'][0]['game']==0 && isset($series[0]['videos'][$video]['youtube_id'])) {
		$start = substr($series[0]["videos"][$video]["youtube_id"], 0, strpos($series[0]["videos"][$video]["youtube_id"], "-"));
		$end = substr($series[0]["videos"][$video]["youtube_id"], strpos($series[0]["videos"][$video]["youtube_id"], "-")+1, strlen($series[0]["videos"][$video]["youtube_id"]));
		echo '<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="//www.youtube.com/embed/' . $series[0]["videos"][0]["youtube_id"] . '?start=' . $start . '&end=' . $end . '&modestbranding=1" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>';
	}
	else echo '<div class="embed-responsive embed-responsive-16by9" id="not-played"><h4 class="text-center">Game was not played!</h1></div>'
	?>
	<p class="text-center"><?php echo $series[0]['event'][0]['name'] ?> / cast by: <?php echo $series[0]['channels'][0]['name']; ?></p>
</div>
