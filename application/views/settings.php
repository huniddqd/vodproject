<div class="col-xs-12 col-lg-8 pull-left">
	<div class="page-header">
		<h3>Change E-mail</h3>
	</div>
	<form id="change_form" method="post" class="form-horizontal login col-xs-12 col-sm-10" action="<?php echo base_url();?>index.php/user/change">
		<div class="form-group">
			<label for="new_email" class="col-sm-4 control-label">New e-mail address</label>
			<div class="col-sm-8">
				<input type="email" class="form-control" name="new_email" placeholder="<?php echo $this->session->userdata('email');?>" />
			</div>
		</div>
		<div class="page-header">
			<h3>Change Username</h3>
		</div>
		<div class="form-group">
			<label for="new_username" class="col-sm-4 control-label">New username</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="new_username" placeholder="<?php echo $this->session->userdata('username'); ?>" />
			</div>
		</div>
		<div class="page-header">
			<h3>Change Password</h3>
			<div class="form-group">
				<label for="new_pass" class="col-sm-4 control-label">New Password</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" name="new_pass" placeholder="New Password" />
				</div>
			</div>

			<div class="form-group">
				<label for="new_pass_re" class="col-sm-4 control-label">Re-enter New Password</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" name="new_pass_re" placeholder="New Password" />
				</div>
			</div>

			<div class="form-group">
				<label for="delete" class="col-sm-4 control-label text-danger">I want to delete my account</label>
				
				<div class="col-sm-8"><input type="checkbox" name="delete" value="true" /></div>
			</div>

		</div>
		<div class="form-group">
			<label for="old_pass" class="col-sm-4 control-label">Old Password*</label>
			<div class="col-sm-8">
				<input type="password" class="form-control" name="old_pass" placeholder="Old Password" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6 col-sm-offset-4">
				<button type="submit" class="btn btn-primary pull-left" id="new_change_submit" name="new_change_submit">Submit</button>
			</div>
		</div>
	</form>
</div>	