<div class="row alert_row" id="reg_alert">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-hide="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <span id="reg_alert_massage"></span>
    </div>
  </div>
  <div class="col-md-4"></div>
</div>

<div class="row alert_row" id="reg_succsesful">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="alert alert-success alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-hide="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      Registration successful. You can now log in to this web site.
    </div>
  </div>
  <div class="col-md-4"></div>
</div>

<div class="row alert_row" id="login_succsesful">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="alert alert-success alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-hide="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      Login successful
    </div>
  </div>
  <div class="col-md-4"></div>
</div>

<div class="row alert_row" id="login_alert">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-hide="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <span id="login_alert_massage"></span>
    </div>
  </div>
  <div class="col-md-4"></div>
</div>


<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="login">Login</span><span class="register hidden">Register</span></h4>
      </div>
      <div class="modal-body">
        <!-- The form is placed inside the body of modal -->
        <form id="loginForm" method="post" class="form-horizontal login" action="<?php echo base_url();?>index.php/user/login">
          <div class="form-group">
            <label class="col-sm-4 control-label">E-mail</label>
            <div class="col-sm-6">
              <input type="email" class="form-control" name="login_email" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="login_password" />
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-6 col-sm-offset-4">
              <button type="submit" class="btn btn-primary pull-right">Login</button>
            </div>
          </div>
        </form>

        <form id="registerForm" method="post" class="form-horizontal hidden register" action="<?php echo base_url();?>index.php/user/register">
          <div class="form-group">
            <label class="col-sm-4 control-label">Username</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="reg_username" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">E-mail</label>
            <div class="col-sm-6">
              <input type="email" class="form-control" name="reg_email" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="reg_password1" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Re-enter Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control" name="reg_password2" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-4 control-label">Captcha</label>
            <div class="col-sm-6 captchaContainer">
              <div id="reg_captcha"></div>
            </div>
          </div>         

          <div class="form-group">
            <div class="col-sm-6 col-sm-offset-4">
              <button type="submit" class="btn btn-primary pull-right" id="register_button">Register</button>
            </div>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <span class="login">Don't have an account yet? <a href="#" id="register">Sign up now!</a></span>
        <span class="hidden register">You have an account <a href="#" id="login">Login Here!</a></span>
      </div>
    </div>
  </div>
</div>

<nav class="navbar navbar-default" role="navigation">
  <div class="container">
   <!-- Brand and toggle get grouped for better mobile display -->
   <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
     <span class="sr-only">Toggle navigation</span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
   </button>
   <a class="navbar-brand" href="<?php echo base_url(); ?>">Esport Vods</a>
 </div>

 <!-- Collect the nav links, forms, and other content for toggling -->
 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav">
   <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Games <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
     <li><a href="#">All</a></li>
     <li><a href="#">Counter Strike: Global offensive</a></li>
     <li><a href="#">Dota 2</a></li>
     <li><a href="#">Hearthstone</a></li>
     <li><a href="#">Heroes of the Storm</a></li>
     <li><a href="#">League of Legends</a></li>
     <li><a href="#">Smite</a></li>
     <li><a href="#">Starcraft II</a></li>
     <li><a href="#">World of Tanks</a></li>
   </ul>
 </li>
 <li><a href="#" class="visible-sm">Search</a></li>

</ul>
<form class="navbar-form navbar-left hidden-sm" id="search" role="search" action="<?php echo base_url();?>index.php/search/q/" method="get">
 <div class="form-group">
  <div class="input-group">
   <input type="text" name="querry" class="form-control">
   <span class="input-group-btn">
    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
  </span>
</div><!-- /input-group -->
</div>
</form>
<ul class="nav navbar-nav navbar-right">
  <li><a href="#" data-toggle="modal" data-target="#loginModal">Login</a></li>
</ul>
</div><!-- /.navbar-collapse -->
</div><!-- /.container-fluid -->
</nav>
<div class="container content">