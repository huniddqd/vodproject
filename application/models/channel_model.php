<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Channel_model extends CI_Model {


	public function get_all_channels()
	{
		$query = $this->db->query('SELECT * FROM channels');
		
		return $query->result_array();
	}

	public function delete_channel($channel_id = '')
	{
		if($channel_id != ''){
			$this->db->delete('channels', array('channels_id' => $channel_id)); 
		}
	}

	public function add_channel($channel_id = '', $channel_name = '')
	{
		if($channel_id != '' && $channel_name != ''){
			$data = array(
				'name' => $channel_name ,
				'youtube_channel_id' => $channel_id,
				'name_uri' => url_title($channel_name)
				);

			$this->db->insert('channels', $data); 
		}
	}

	public function get_channel_by_id($channels_id)
	{
		$query = $this->db->get_where('channels', array('channels_id' => $channels_id));
		return $query->result_array();
	}

	public function get_id_by_uri($channel_name_uri = '')
	{
		$this->db->from('channels');
		$this->db->where('name_uri', $channel_name_uri);
		$query = $this->db->get();

		return $query->result_array();
	}


}

/* End of file even_model.php */
/* Location: ./application/models/even_model.php */
