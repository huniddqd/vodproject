<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends CI_Model {


	public function get_all_event()
	{
		$query = $this->db->get('events');
		return $query->result_array();
	}

	public function create_event($data)
	{
		$this->db->insert('events', $data);
	}

	public function get_id($event)
	{
		$query = $this->db->get_where('events', array('name' => $event), 1);
		return $query->result_array();
	}
	
	public function add_channel($channel)
	{
		$this->db->insert('channels_events_look_up', $channel);
	}

	public function get_channels($event_id)
	{
		$this->db->from('channels_events_look_up');
		$this->db->join('channels', 'channels_events_look_up.channels_id = channels.channels_id');
		$this->db->where('events_id', $event_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_by_id($events_id)
	{
		$this->db->from('events');
		$this->db->where('events_id', $events_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_id_by_uri($event_name_uri = '')
	{
		$this->db->from('events');
		$this->db->where('name_uri', $event_name_uri);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function remove_channel($channel)
	{
		$this->db->delete(
			'channels_events_look_up',
			array(
				'channels_id' => $channel['channels_id'], 
				'events_id' => $channel['events_id']
				)
			);
	}
}

/* End of file even_model.php */
/* Location: ./application/models/even_model.php */
