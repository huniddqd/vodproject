<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class video_model extends CI_Model {


	public function get_all_videos()
	{
		$query = $this->db->get('videos');
		return $query->result_array();
	}

	public function add_video($data)
	{
		$this->db->insert('videos', $data);
	}

	public function get_id($video)
	{
		$query = $this->db->get_where('videos', array('name' => $video), 1);
		return $query->result_array();
	}

	public function exist_by_youtube_id($youtube_id)
	{
		$query = $this->db->get_where('videos', array('youtube_id' => $youtube_id));
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	public function get_videos_by_Series($series_id)
	{
		$this->db->from('videos');
		$this->db->order_by('game', 'asc');
		$this->db->where('series_id', $series_id);
		$query = $this->db->get();
		return $query->result_array();
	}

}

/* End of file even_model.php */
/* Location: ./application/models/even_model.php */
