<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Youtube_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
	}

	public function get_all_videos($youtube='')
	{

		$searchResponse = $youtube->search->listSearch('id,snippet', array(
			'q' => $_GET['q'],
			'maxResults' => $_GET['maxResults'],
			));
		print_r($searchResponse);
	}

	public function get_new_videos($youtube, $channel, $token, $q)
	{
		try {
			$searchResponse = $youtube->search->listSearch('id,snippet', array(
				'channelId' => $channel,	
				'videoEmbeddable' => 'true',
				'maxResults' => 50,
				'type' => 'video',
				'order' => 'date',
				'q' => $q,
				'publishedAfter' => date(DATE_RFC3339,mktime (0,0,0,1,1,2015)),
				'pageToken' => $token
				));
		}
		catch (Exception $e) {
			echo "<pre>";
			print_r ($e);
			echo "</pre>";
		}

		return ($searchResponse);
	}

	public function get_channel_by_id($channel_id, $youtube)
	{
		try {
			$searchResponse = $youtube->channels->listChannels('snippet',array(
				'id' => $channel_id,
				'maxResults' => 1
				));	
		} catch (Exception $e) {
			echo "<pre>";
			print_r ($e);
			echo "</pre>";
		}

		return ($searchResponse);
	}
}

/* End of file youtube_model.php */
/* Location: ./application/models/youtube_model.php */