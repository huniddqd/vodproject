<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Series_model extends CI_Model {

	public function get_open_series()
	{
		$this->db->select('series.series_id, series.name, series.games_id, series.bo, series.closed, series.events_id, games.games_id, games.game, games.icon, events.events_id, events.name as events_name');
		$this->db->from('series');
		$this->db->join('games', 'games.games_id = series.games_id');
		$this->db->join('events', 'events.events_id = series.events_id');
		$this->db->where('series.closed', '0');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_all_series(){
		$this->db->select('series.series_id, series.name, series.games_id, series.bo, series.closed, series.events_id, series.channels_id, games.games_id, games.game, games.icon, events.events_id, events.name as events_name,');
		$this->db->from('series');
		$this->db->join('games', 'games.games_id = series.games_id');
		$this->db->join('events', 'events.events_id = series.events_id');
		$this->db->join('channels', 'channels.channels_id = series.channels_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_closed_series(){
		$this->db->select('series.series_id, series.name, series.games_id, series.bo, series.closed, series.events_id, series.channels_id, games.games_id, games.game, games.icon, events.events_id, events.name as events_name,');
		$this->db->from('series');
		$this->db->join('games', 'games.games_id = series.games_id');
		$this->db->join('events', 'events.events_id = series.events_id');
		$this->db->join('channels', 'channels.channels_id = series.channels_id');
		$this->db->where('series.closed', '1');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_series_by_event($events_id)
	{
		$this->db->select('series.series_id, series.name, series.games_id, series.bo, series.closed, series.events_id, series.channels_id, games.games_id, games.game, games.icon, events.events_id, events.name as events_name,');
		$this->db->from('series');
		$this->db->join('games', 'games.games_id = series.games_id');
		$this->db->join('events', 'events.events_id = series.events_id');
		$this->db->join('channels', 'channels.channels_id = series.channels_id');
		$this->db->where('series.closed', '1');
		$this->db->where('events.events_id', $events_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_series_by_channel($channel_id)
	{
		$this->db->select('series.series_id, series.name, series.games_id, series.bo, series.closed, series.events_id, series.channels_id, games.games_id, games.game, games.icon, events.events_id, events.name as events_name,');
		$this->db->from('series');
		$this->db->join('games', 'games.games_id = series.games_id');
		$this->db->join('events', 'events.events_id = series.events_id');
		$this->db->join('channels', 'channels.channels_id = series.channels_id');
		$this->db->where('series.closed', '1');
		$this->db->where('channels.channels_id', $channel_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function create_series($data)
	{
		$this->db->insert('series', $data);
	}

	public function close($series_id = '')
	{
		if($series_id != ''){
			$data = array('closed' => true, );
			$this->db->where('series_id', $series_id);
			$this->db->update('series', $data); 
		}
	}

	public function get_by_id($id ='')
	{
		if($id != ''){
			$query = $this->db->get_where('series', array('series_id' => $id), 1);
			return $query->result_array();
		}
	}

	public function add_player($data)
	{
		$this->db->insert('players_series_look_up', $data);
	}

	public function remove_player($data)
	{
		$this->db->delete('players_series_look_up', $data); 
	}

}

/* End of file series_model.php */
/* Location: ./application/models/series_model.php */