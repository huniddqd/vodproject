<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function add_user($email, $username, $password)
    {

        $options = [
        'cost' => 12
        ];

        $data = array(
            'email' => $email,
            'username' => $username,
            'password' => password_hash($password, PASSWORD_DEFAULT, $options)
            );

        return $this->db->insert('users', $data); 
    }

    public function delete($id)
    {
        $this->db->delete('login_attempts', array('user_id' => $id)); 
        $this->db->delete('users', array('users_id' => $id)); 
    }

    public function login_user($email, $password)
    {
        $query = $this->db->query('SELECT password, users_id FROM vodusers WHERE email = \'' . $email . '\' LIMIT 1');
        $password_db = '';
        $user_id = '';
        $data = array(
            'successful' => false,
            'user_id' => 0
            );


        if ($query->num_rows() > 0){
            foreach ($query->result() as $row)
            {
                $password_db = $row->password;
                $user_id = $row->users_id;
            }
            $data['user_id'] = $user_id;

        }else{
            $this->db->insert('login_attempts', $data);
            return -1;
        }

        if (password_verify($password, $password_db)) {
            $data['successful'] = true;
            $this->db->insert('login_attempts', $data);
            return $user_id;
        }
        $this->db->insert('login_attempts', $data);
        return -1;
    }

    public function get_username($email)
    {
        $username = '';
        $query = $this->db->query('SELECT username FROM vodusers WHERE email = \'' . $email . '\'');
        if($query->num_rows() > 0 ){
            foreach ($query->result() as $row) {
                $username = $row->username;
            }
        }
        return $username;
    }

    public function get_userid($email)
    {
        $userid = '';
        $query = $this->db->query('SELECT users_id FROM vodusers WHERE email = \'' . $email . '\'');
        if($query->num_rows() > 0 ){
            foreach ($query->result() as $row) {
                $userid = $row->users_id;
            }
        }
        return $userid;
    }

    public function get_privilage($email)
    {
        $userprivilage = '';
        $query = $this->db->query('SELECT privilage FROM vodusers WHERE email = \'' . $email . '\'');
        if($query->num_rows() > 0 ){
            foreach ($query->result() as $row) {
                $userprivilage = $row->privilage;
            }
        }
        if ($userprivilage == 1){
            return true;
        }else{
            return false;
        }
    }

    public function bruteforce($email)
    {
        $query = $this->db->query('SELECT * From vodlogin_attempts INNER JOIN vodusers ON vodlogin_attempts.user_id = vodusers.users_id WHERE vodusers.email = \'' . $email . '\' AND time > NOW() - INTERVAL 15 MINUTE AND vodlogin_attempts.successful = 0');
        if($query->num_rows() >= 3){
            return true;
        }else{
            return false;
        }
    }
    public function change($change ,$type = "")
    {
        if($change !=""){
            if($type != "pass"){
                $data = array(
                    $type => $change
                    );
                $this->session->set_userdata( $data );
            }else{
                $options = [
                'cost' => 12
                ];
                $data = array(
                    'password' => password_hash($change, PASSWORD_DEFAULT, $options)
                    );
            }
            $this->db->where('users_id', $this->session->userdata('user_id'));
            $this->db->update('users', $data);

        }else{
            echo "Something went wrong!";
        }
    }
    
    public function get_all_users()
    {
        $query = $this->db->query('SELECT * FROM vodusers');
        
        return $query->result();
    }
}