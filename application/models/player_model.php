<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player_model extends CI_Model {


	public function get_all_player()
	{
		$query = $this->db->get('players');
		return $query->result_array();
	}

	public function create_player($data)
	{
		$this->db->insert('players', $data);
	}

	public function get_id($player)
	{
		$query = $this->db->get_where('players', array('name' => $player), 1);
		return $query->result_array();
	}

	public function get_by_id($id ='')
	{
		if($id != ''){
			$query = $this->db->get_where('players', array('players_id' => $id), 1);
			return $query->result_array();
		}
	}
	
	public function get_players_by_series($series_id)
	{
		$this->db->from('players_series_look_up');
		$this->db->join('players', 'players_series_look_up.players_id = players.players_id');
		$this->db->where('series_id', $series_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function remove_player($data)
	{
		$this->db->delete('players', $data); 
	}

}

/* End of file even_model.php */
/* Location: ./application/models/even_model.php */
