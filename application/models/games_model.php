<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Games_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all_games()
	{
		$query = $this->db->get('games');
		return $query->result_array();
	}

	public function get_id($game)
	{
		$query = $this->db->get_where('games', array('game' => $game), 1);
		return $query->result_array();
	}

	public function get_by_id($id ='')
	{
		if($id != ''){
			$query = $this->db->get_where('games', array('games_id' => $id), 1);
			return $query->result_array();
		}
	}

}

/* End of file game_model.php */
/* Location: ./application/models/game_model.php */
