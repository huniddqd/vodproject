<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function player($query ='')
	{
		$this->db->from('players');
		$this->db->like('name', $query, 'both');
		$this->db->limit(3);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function event($query ='')
	{
		$this->db->from('events');
		$this->db->like('name', $query, 'both');
		$this->db->limit(3);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function channel($query ='')
	{
		$this->db->from('channels');
		$this->db->like('name', $query, 'both');
		$this->db->limit(3);
		$result = $this->db->get();
		return $result->result_array();
	}

	public function series($query ='')
	{
		$this->db->from('series');
		$this->db->like('name', $query, 'both');
		$this->db->limit(3);
		$result = $this->db->get();
		return $result->result_array();
	}

}

/* End of file search_model.php */
/* Location: ./application/models/search_model.php */