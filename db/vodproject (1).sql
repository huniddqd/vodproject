-- phpMyAdmin SQL Dump
-- version 4.0.9deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2015 at 01:56 PM
-- Server version: 5.5.33-1
-- PHP Version: 5.5.6-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vodproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `vodchannels`
--

CREATE TABLE IF NOT EXISTS `vodchannels` (
  `channels_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `youtube_channel_id` varchar(24) NOT NULL,
  `name_uri` text NOT NULL,
  PRIMARY KEY (`channels_id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `channel_id` (`youtube_channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `vodchannels`
--

INSERT INTO `vodchannels` (`channels_id`, `name`, `youtube_channel_id`, `name_uri`) VALUES
(8, 'BaseTradeTV', 'UCm-iYhOKdXiPe2sUngE50fQ', 'BaseTradeTV'),
(9, 'TotalBiscuit Starcraft and Axiom eSports', 'UC3kJdy9_bXFg8flaSY3RAcQ', 'TotalBiscuit-Starcraft-and-Axiom-eSports'),
(11, 'ESL', 'UC0G2qz-hoaCswQNgoWU_LTw', 'ESL');

-- --------------------------------------------------------

--
-- Table structure for table `vodchannels_events_look_up`
--

CREATE TABLE IF NOT EXISTS `vodchannels_events_look_up` (
  `channels_events_look_up_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `channels_id` bigint(20) unsigned NOT NULL,
  `events_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`channels_events_look_up_id`),
  KEY `events_id` (`events_id`),
  KEY `channels_id` (`channels_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `vodchannels_events_look_up`
--

INSERT INTO `vodchannels_events_look_up` (`channels_events_look_up_id`, `channels_id`, `events_id`) VALUES
(1, 8, 36),
(4, 8, 33),
(5, 8, 34),
(6, 9, 37),
(7, 11, 38);

-- --------------------------------------------------------

--
-- Table structure for table `vodevents`
--

CREATE TABLE IF NOT EXISTS `vodevents` (
  `events_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `name_uri` text NOT NULL,
  PRIMARY KEY (`events_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `vodevents`
--

INSERT INTO `vodevents` (`events_id`, `name`, `name_uri`) VALUES
(33, 'The Safety Net', 'The-Safety-Net'),
(34, 'Two vs Twournament', 'Two-vs-Twournament'),
(36, 'OlimoLeague #20', 'OlimoLeague-20'),
(37, 'Axiom LOTV Tournament', 'Axiom-LOTV-Tournament'),
(38, 'Legendary Series Finals', 'Legendary-Series-Finals');

-- --------------------------------------------------------

--
-- Table structure for table `vodevents_games_look_up`
--

CREATE TABLE IF NOT EXISTS `vodevents_games_look_up` (
  `events_games_lookup_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `evenets_id` bigint(20) unsigned NOT NULL,
  `games_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`events_games_lookup_id`),
  KEY `evenets_id` (`evenets_id`),
  KEY `games_id` (`games_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vodgames`
--

CREATE TABLE IF NOT EXISTS `vodgames` (
  `games_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  PRIMARY KEY (`games_id`),
  UNIQUE KEY `game` (`game`,`icon`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `vodgames`
--

INSERT INTO `vodgames` (`games_id`, `game`, `icon`) VALUES
(1, 'Counter Strike: Global offensive', 'csgo_icon.png'),
(2, 'Dota 2', 'dota_2_icon.png'),
(3, 'Hearthstone', 'hearthstone_icon.png'),
(4, 'Heroes of the Storm', 'hots_icon.png'),
(5, 'League of Legends', 'lol_icon.png'),
(6, 'Smite', 'smite_icon.png'),
(7, 'Starcraft II', 'sc2_icon.png'),
(8, 'World of Tanks', 'worldoftanks_icon.png');

-- --------------------------------------------------------

--
-- Table structure for table `vodlogin_attempts`
--

CREATE TABLE IF NOT EXISTS `vodlogin_attempts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `successful` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=359 ;

--
-- Dumping data for table `vodlogin_attempts`
--

INSERT INTO `vodlogin_attempts` (`id`, `user_id`, `time`, `successful`) VALUES
(319, 6, '2015-02-12 18:00:16', 1),
(320, 6, '2015-02-12 18:27:03', 1),
(321, 6, '2015-02-12 18:49:57', 1),
(322, 6, '2015-02-12 20:01:10', 1),
(323, 6, '2015-02-12 20:06:52', 1),
(331, 6, '2015-02-12 22:24:14', 0),
(332, 6, '2015-02-12 22:24:19', 1),
(343, 6, '2015-02-12 22:36:46', 1),
(344, 6, '2015-02-13 16:34:49', 1),
(345, 6, '2015-02-18 15:33:20', 1),
(346, 6, '2015-02-18 17:28:40', 1),
(347, 6, '2015-02-23 18:33:09', 1),
(348, 6, '2015-02-25 13:41:25', 1),
(349, 6, '2015-02-26 14:06:14', 1),
(350, 6, '2015-03-02 15:45:20', 1),
(351, 6, '2015-03-02 18:03:10', 1),
(352, 6, '2015-03-06 14:04:58', 1),
(353, 6, '2015-03-06 14:26:47', 1),
(354, 6, '2015-03-06 14:51:25', 1),
(355, 36, '2015-03-06 14:55:25', 1),
(356, 36, '2015-03-06 14:56:34', 1),
(357, 36, '2015-03-06 18:54:09', 1),
(358, 36, '2015-05-18 11:19:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vodplayers`
--

CREATE TABLE IF NOT EXISTS `vodplayers` (
  `players_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `picture` varchar(50) NOT NULL,
  PRIMARY KEY (`players_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `vodplayers`
--

INSERT INTO `vodplayers` (`players_id`, `name`, `picture`) VALUES
(4, 'Catz', 'default.jpg'),
(5, 'JonSnow', 'default.jpg'),
(6, 'Harstem', 'default.jpg'),
(7, 'Nerchio', 'default.jpg'),
(8, 'XenoCider', 'default.jpg'),
(9, 'Minigun', 'default.jpg'),
(10, 'Lilbow', 'default.jpg'),
(11, 'Krass', 'default.jpg'),
(12, 'State', 'default.jpg'),
(13, 'Tyga', 'default.jpg'),
(16, 'Siphonn', 'default.jpg'),
(17, 'Patience', 'default.jpg'),
(18, 'True', 'default.jpg'),
(19, 'Ryung', 'default.jpg'),
(20, 'Impact', 'default.jpg'),
(21, 'Crank', 'default.jpg'),
(22, 'Heart', 'default.jpg'),
(23, 'SuperNova', 'default.jpg'),
(24, 'Symbol', 'default.jpg'),
(26, 'Solar', 'default.jpg'),
(27, 'Stork', 'default.jpg'),
(28, 'Alicia', 'default.jpg'),
(30, 'MMA', 'default.jpg'),
(32, 'TheStC', 'default.jpg'),
(34, 'Miya', 'default.jpg'),
(35, 'Zalae', 'default.jpg'),
(36, 'SilentStorm', 'default.jpg'),
(37, 'Savjz', 'default.jpg'),
(38, 'Max', 'default.jpg'),
(39, 'Darkwonyx', 'default.jpg'),
(40, 'PinpingHo', 'default.jpg'),
(41, 'Chakki', 'default.jpg'),
(42, 'Weifu', 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `vodplayers_series_look_up`
--

CREATE TABLE IF NOT EXISTS `vodplayers_series_look_up` (
  `players_series_look_up_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `players_id` bigint(20) unsigned NOT NULL,
  `series_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`players_series_look_up_id`),
  KEY `players_id` (`players_id`),
  KEY `series_id` (`series_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `vodplayers_series_look_up`
--

INSERT INTO `vodplayers_series_look_up` (`players_series_look_up_id`, `players_id`, `series_id`) VALUES
(7, 4, 1),
(8, 12, 1),
(9, 5, 4),
(10, 4, 4),
(11, 5, 5),
(12, 13, 5),
(13, 12, 6),
(14, 5, 6),
(15, 6, 7),
(16, 7, 7),
(17, 8, 8),
(18, 6, 8),
(19, 9, 9),
(20, 10, 9),
(22, 11, 10),
(23, 12, 10),
(24, 13, 11),
(25, 4, 11),
(26, 17, 3),
(27, 18, 3),
(28, 21, 3),
(29, 22, 3),
(30, 17, 12),
(31, 18, 12),
(32, 19, 12),
(33, 20, 12),
(34, 22, 13),
(35, 21, 13),
(36, 19, 13),
(37, 20, 13),
(38, 21, 14),
(39, 22, 14),
(40, 17, 14),
(41, 18, 14),
(42, 19, 15),
(43, 20, 15),
(45, 23, 15),
(46, 24, 15),
(47, 23, 16),
(48, 24, 16),
(49, 21, 16),
(50, 22, 16),
(51, 26, 17),
(52, 27, 17),
(53, 17, 17),
(54, 18, 17),
(55, 34, 18),
(56, 28, 18),
(57, 20, 19),
(58, 30, 19),
(59, 21, 20),
(60, 19, 20),
(61, 22, 21),
(62, 32, 21),
(63, 20, 22),
(64, 28, 22),
(65, 21, 23),
(66, 22, 23),
(67, 20, 24),
(68, 21, 24),
(69, 40, 25),
(70, 42, 25),
(71, 35, 26),
(72, 36, 26),
(73, 36, 27),
(74, 37, 27),
(75, 35, 28),
(76, 38, 28),
(77, 41, 29),
(78, 39, 29),
(79, 37, 30),
(80, 38, 30),
(81, 35, 31),
(82, 41, 31),
(83, 41, 32),
(84, 42, 32),
(85, 40, 34),
(86, 41, 34),
(88, 39, 35),
(89, 40, 35),
(90, 41, 36),
(91, 36, 36),
(92, 42, 37),
(93, 41, 37),
(94, 39, 38),
(95, 36, 38),
(96, 36, 41),
(97, 37, 41),
(98, 40, 42),
(99, 41, 42),
(100, 35, 43),
(101, 41, 43),
(102, 38, 44),
(103, 37, 44);

-- --------------------------------------------------------

--
-- Table structure for table `vodseries`
--

CREATE TABLE IF NOT EXISTS `vodseries` (
  `series_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `games_id` bigint(20) unsigned NOT NULL,
  `bo` int(11) NOT NULL,
  `closed` text NOT NULL,
  `events_id` bigint(20) unsigned NOT NULL,
  `channels_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`series_id`),
  UNIQUE KEY `name` (`name`),
  KEY `games_id` (`games_id`),
  KEY `events_id` (`events_id`),
  KEY `channels_id` (`channels_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `vodseries`
--

INSERT INTO `vodseries` (`series_id`, `name`, `games_id`, `bo`, `closed`, `events_id`, `channels_id`) VALUES
(1, 'Catz vs State - The Safety Net', 7, 3, '1', 33, 8),
(3, 'Patience/True vs Crank/Heart [FINALS] - Two vs Twournament', 7, 5, '1', 34, 8),
(4, 'JonSnow vs Catz - The Safety Net', 7, 3, '1', 33, 8),
(5, 'JonSnow vs Tyga - The Safety Net', 7, 3, '1', 33, 8),
(6, 'State vs JonSnow - The Safety Net', 7, 3, '1', 33, 8),
(7, 'Harstem vs Nerchio - The Safety Net', 7, 5, '1', 33, 8),
(8, 'XenoCider vs Harstem G2 - The Safety Net', 7, 5, '1', 33, 8),
(9, 'Minigun vs Lilbow', 7, 5, '1', 33, 8),
(10, 'Krass vs State ', 7, 5, '1', 33, 8),
(11, 'Tyga vs Catz - The Safety Net', 7, 3, '1', 33, 8),
(12, 'Patience/True vs Ryung/Impact G2 - Two vs Twournament', 7, 3, '0', 34, 8),
(13, 'Crank/Heart vs Ryung/Impact - Two vs Twournament', 7, 3, '1', 34, 8),
(14, 'Crank/Heart vs Patience/True - Two vs Twournament', 7, 3, '1', 34, 8),
(15, 'Ryung/Impact vs SuperNova/Symbol - Two vs Twournament', 7, 3, '0', 34, 8),
(16, 'SuperNova/Symbol vs Crank/Heart - Two vs Twournament', 7, 3, '1', 34, 8),
(17, 'Solar/Stork vs Patience/True - Two vs Twournament', 7, 3, '1', 34, 8),
(18, 'Axiom LOTV Tournament - RO8 - Miya vs. Alicia', 7, 3, '1', 37, 9),
(19, 'Axiom LOTV Tournament - RO8 - Impact vs. MMA', 7, 3, '1', 37, 9),
(20, 'Axiom LOTV Tournament - RO8 - CranK vs. Ryung', 7, 3, '1', 37, 9),
(21, 'Axiom LOTV Tournament - RO8 - Heart vs. TheSTC', 7, 3, '1', 37, 9),
(22, 'Axiom LOTV Tournament - Semifinal 1', 7, 3, '1', 37, 9),
(23, 'Axiom LOTV Tournament - Semifinal 2', 7, 3, '1', 37, 9),
(24, 'Axiom LOTV Tournament - Grand Final', 7, 5, '1', 37, 9),
(25, 'Hearthstone - PinpingHo vs. Weifu - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(26, 'Zalae vs. SilentStorm - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(27, 'SilentStorm vs. Savjz - ESL Legendary Series Finals ', 3, 5, '1', 38, 11),
(28, 'Zalae vs. Max - ESL Legendary Series Final', 3, 5, '1', 38, 11),
(29, 'Chakki vs. Darkwonyx - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(30, 'Savjz vs. Max - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(31, 'Zalae vs. Chakki - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(32, 'Chakki vs. Weifu - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(34, 'PinpingHo vs. Chakki - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(35, 'Darkwonyx vs. PinpingHo - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(36, 'Chakki vs. SilentStorm - ESL Legendary Series Final', 3, 5, '1', 38, 11),
(37, 'Weifu vs. Chakki - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(38, ' Darkwonyx vs. SilentStorm - ESL Legendary Series Finals', 3, 5, '1', 38, 11),
(41, 'Hearthstone - SilentStorm vs. Savjz - ESL Legendary Series Finals - Quarterfinal', 3, 5, '1', 38, 11),
(42, 'Hearthstone - Pinpingho vs. Chakki - ESL Legendary Series Finals - Quarterfinal', 3, 5, '1', 38, 11),
(43, 'Hearthstone - Zalae vs. Chakki - ESL Legendary Series Finals - Semifinal', 3, 5, '1', 38, 11),
(44, 'Hearthstone - Max vs. Savjz - ESL Legendary Series Finals - Quarterfinal', 3, 5, '1', 38, 11);

-- --------------------------------------------------------

--
-- Table structure for table `vodusers`
--

CREATE TABLE IF NOT EXISTS `vodusers` (
  `users_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `username` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `privilage` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`users_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `vodusers`
--

INSERT INTO `vodusers` (`users_id`, `email`, `username`, `password`, `privilage`) VALUES
(6, 'kaloczi.gabor@gmail.com', 'iddqd', '$2y$12$zNGuc2O3HnrC7RW4zRikRu0cEnQptF1qZBwZ7w8q60dTDEUeiuEqi', 1),
(36, 'asdf@asdf.hu', 'asdf', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(37, 'qwer@qwer.hu', 'qwer', '$2y$12$DMJwRnFkR0keFj5woGTi.Oo6qEz2/fILvMrGSGD.h46yqBdmoxNVy', 0),
(42, 'risus.Donec@netusetmalesuada.co.uk', 'Kasimir', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(43, 'ullamcorper.magna.Sed@consectetuer.com', 'Hadley', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(44, 'sed.facilisis.vitae@molestiesodalesMauris.co.uk', 'Renee', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(49, 'Aenean.euismod@Loremipsumdolor.net', 'Sarah', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(50, 'ac.metus.vitae@Curabitursed.co.uk', 'Francis', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(52, 'dictum.ultricies.ligula@magnamalesuadavel.co.uk', 'Dean', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(53, 'molestie.Sed@necmetusfacilisis.co.uk', 'Orson', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(54, 'amet.consectetuer@magnaaneque.co.uk', 'Desiree', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(55, 'ac.facilisis.facilisis@etmalesuadafames.ca', 'Tarik', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(56, 'at.sem.molestie@arcuet.co.uk', 'Lacey', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(58, 'ipsum.dolor.sit@Nullatinciduntneque.com', 'Driscoll', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(59, 'varius@Curabitur.com', 'Indira', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(60, 'Proin@nonummyultriciesornare.edu', 'Brett', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(61, 'molestie.pharetra.nibh@Sedeunibh.co.uk', 'Deborah', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(62, 'eu.ultrices@consequatdolor.org', 'Basil', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(63, 'penatibus@Fuscedolor.co.uk', 'Samuel', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(64, 'pede.Praesent.eu@pede.org', 'Cathleen', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(65, 'risus@metuseu.co.uk', 'Mechelle', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(66, 'id.ante.dictum@dolorFusce.org', 'Leila', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(67, 'ridiculus.mus@faucibusleo.net', 'Elvis', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(68, 'Integer.mollis@ligulaelitpretium.edu', 'Ashely', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(69, 'nunc.ac.mattis@Sed.ca', 'Keane', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(70, 'libero@Cumsociis.com', 'Hayes', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(71, 'lorem.ut.aliquam@enimnec.ca', 'Jarrod', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(72, 'Donec.dignissim.magna@enim.com', 'Ivory', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(73, 'congue.turpis.In@sitamet.ca', 'Faith', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(74, 'sociis.natoque.penatibus@lacinia.net', 'Erich', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(75, 'Suspendisse@mus.net', 'Elton', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(76, 'a.magna@tristiquesenectuset.co.uk', 'Lester', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(77, 'faucibus.orci.luctus@Integersemelit.org', 'Virginia', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(78, 'pellentesque@Aliquam.edu', 'Bree', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(79, 'non.hendrerit.id@nonluctus.ca', 'Charissa', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(80, 'lorem.eu@luctus.com', 'Abel', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(81, 'quam.vel@atrisus.net', 'India', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(82, 'enim@eunullaat.ca', 'Hop', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(83, 'mollis@felisorci.co.uk', 'Ori', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(84, 'ac.mattis@tortorNunc.ca', 'Jennifer', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(85, 'porttitor.eros@Curae.edu', 'Fritz', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(86, 'ante.ipsum@cursusvestibulum.ca', 'Hector', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(87, 'dictum@Inornaresagittis.org', 'Timothy', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(88, 'nec.euismod@Crasconvallisconvallis.net', 'Hakeem', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(89, 'metus@sagittisplacerat.com', 'Jonah', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(90, 'elit.Aliquam@semper.net', 'Jane', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(91, 'mauris.rhoncus@et.com', 'Benedict', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(92, 'nec.mollis.vitae@ipsumac.org', 'Bradley', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(93, 'auctor.ullamcorper@ante.org', 'Paula', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(94, 'eget.laoreet.posuere@nullaatsem.com', 'Germaine', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(95, 'facilisis.eget.ipsum@etipsumcursus.edu', 'Halee', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(96, 'libero@risusIn.net', 'Destiny', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(97, 'Nunc@orcisem.edu', 'Gloria', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(98, 'mauris@morbitristique.org', 'Aladdin', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(99, 'sit.amet.lorem@diameu.com', 'Michael', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(100, 'malesuada.fames@malesuadautsem.co.uk', 'Jaquelyn', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(101, 'mus.Proin@vellectusCum.com', 'Porter', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(102, 'adipiscing@telluseuaugue.org', 'Dora', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(103, 'Proin.ultrices.Duis@suscipitnonummyFusce.com', 'Jamalia', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(104, 'amet@luctus.net', 'Bruno', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(105, 'senectus.et@Nulla.org', 'Sebastian', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(106, 'Mauris@iaculis.com', 'Octavia', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(107, 'convallis@sedconsequat.org', 'Vernon', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(108, 'ad@risusNulla.co.uk', 'Pandora', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(109, 'In@velit.com', 'Justine', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(110, 'sem.Pellentesque.ut@euligulaAenean.org', 'Tiger', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(111, 'conubia.nostra@neccursus.com', 'Dawn', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(112, 'Morbi.neque@Cras.co.uk', 'Deirdre', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(113, 'sed.tortor@Suspendisse.net', 'Yuli', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(114, 'Pellentesque@accumsan.edu', 'Leila', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(115, 'rutrum@tellus.ca', 'Carly', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(116, 'rhoncus@adipiscingnonluctus.org', 'Sacha', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(117, 'neque.Nullam@nonsollicitudin.org', 'Zeus', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(118, 'semper.Nam.tempor@Etiambibendumfermentum.ca', 'Ivan', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(119, 'in.faucibus@egetmetus.com', 'Isaac', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(120, 'at.pretium.aliquet@risusNulla.org', 'Michael', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(121, 'interdum@arcuAliquam.co.uk', 'Alvin', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(122, 'arcu.et.pede@semper.net', 'Mannix', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(123, 'semper.egestas.urna@aliquetPhasellusfermentum.com', 'Ivory', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(124, 'lacus.varius.et@nonmagnaNam.org', 'Uma', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(125, 'mi.Aliquam.gravida@Aeneansed.co.uk', 'Jade', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(126, 'malesuada.augue.ut@eueuismodac.ca', 'Faith', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(127, 'eu@et.co.uk', 'Oleg', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(128, 'eget@tellus.com', 'Nita', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(129, 'massa.lobortis.ultrices@velit.org', 'Pearl', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(130, 'Nullam.feugiat.placerat@ullamcorpernisl.edu', 'Salvador', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(131, 'justo.eu@et.net', 'Martin', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(132, 'ligula.Aliquam.erat@venenatislacusEtiam.net', 'Rooney', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(133, 'orci.lobortis@Donecest.net', 'Sharon', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(134, 'cursus.vestibulum@pellentesqueeget.com', 'Alexis', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(135, 'aliquam.adipiscing.lacus@iaculisquis.net', 'Nicole', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(136, 'orci@commodoauctorvelit.org', 'Amena', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(137, 'neque.Nullam.nisl@sitamet.net', 'Ginger', '$2y$12$ef22V3Y5hBwDudScJtz6Wevznx/F3v46371dfyzH/YxtGU8F2DMLe', 0),
(138, 'qwerw@654321.hu', 'qwerw', '$2y$12$kW/6Nani6ddnY22izxcHT.MVZjW466S07yfLJiu9bnAS.1GXdpCUy', 0);

-- --------------------------------------------------------

--
-- Table structure for table `voduser_sessions`
--

CREATE TABLE IF NOT EXISTS `voduser_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_id` (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voduser_sessions`
--

INSERT INTO `voduser_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('236c99c280123feadb126aeeef79623e', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36', 1431947871, 'a:5:{s:7:"user_id";s:2:"36";s:8:"username";s:4:"asdf";s:5:"admin";b:0;s:5:"email";s:12:"asdf@asdf.hu";s:8:"loged_in";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `vodvideos`
--

CREATE TABLE IF NOT EXISTS `vodvideos` (
  `videos_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `series_id` bigint(20) unsigned NOT NULL,
  `youtube_id` varchar(15) NOT NULL,
  `game` int(11) NOT NULL,
  `title` text NOT NULL,
  PRIMARY KEY (`videos_id`),
  UNIQUE KEY `youtube_id` (`youtube_id`),
  KEY `series_id` (`series_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=120 ;

--
-- Dumping data for table `vodvideos`
--

INSERT INTO `vodvideos` (`videos_id`, `series_id`, `youtube_id`, `game`, `title`) VALUES
(12, 1, 'eiedJ__Logs', 1, 'Catz vs State G1 - The Safety Net'),
(14, 1, 'l9_mHjTyRnU', 2, 'Catz vs State G2 - The Safety Net'),
(15, 3, '5rCpHEBm7WU', 5, 'Patience/True vs Crank/Heart G5 [FINALS] - Two vs Twournament'),
(16, 3, 'ggyA_98Obfo', 2, 'Patience/True vs Crank/Heart G2[FINALS] - Two vs Twournament'),
(17, 3, '07VcKasi1bQ', 4, 'Patience/True vs Crank/Heart G4 [FINALS] - Two vs Twournament'),
(18, 3, 'T9i-E24X_uU', 3, 'Patience/True vs Crank/Heart G3 [FINALS] - Two vs Twournament'),
(19, 3, 'FrKzyks6dmM', 1, 'Patience/True vs Crank/Heart G1 [FINALS] - Two vs Twournament'),
(20, 4, 'hhzftHmRoPE', 3, 'JonSnow vs Catz G3 - The Safety Net'),
(21, 4, 'wNksqV2CTPA', 2, 'JonSnow vs Catz G2 - The Safety Net'),
(22, 4, 'ittw2JJFuC8', 1, 'JonSnow vs Catz G1 - The Safety Net'),
(23, 5, '2jnA-i6-z7Y', 1, 'JonSnow vs Tyga G1 - The Safety Net'),
(24, 5, 'Q-J14Eue6Kg', 3, 'JonSnow vs Tyga G3 - The Safety Net'),
(25, 5, '7ztIZi_ES6k', 2, 'JonSnow vs Tyga G2 - The Safety Net'),
(26, 6, '2omy3k6R79E', 2, 'State vs JonSnow G2 - The Safety Net'),
(27, 6, 'pO1rCQdipe0', 1, 'State vs JonSnow G1 - The Safety Net'),
(28, 7, '3hAUA3W9gSI', 2, 'Harstem vs Nerchio G2 - Semifinal 1 - The Safety Net'),
(29, 7, 'KvAlTmxacRo', 3, 'Harstem vs Nerchio G3 - Semifinal 1 - The Safety Net'),
(30, 7, 'LAAeyHhaM04', 1, 'Harstem vs Nerchio G1 - Semifinal 1 - The Safety Net'),
(31, 8, 'yUMMo85IQ9c', 2, 'XenoCider vs Harstem G2 - The Safety Net'),
(32, 8, 'bJ4F8_EeGns', 4, 'XenoCider vs Harstem G4 - The Safety Net'),
(33, 8, 'KJYkiUi5j44', 1, 'XenoCider vs Harstem G1 - The Safety Net'),
(34, 8, '51gvdvMDYFo', 3, 'XenoCider vs Harstem G3 - The Safety Net'),
(35, 9, 'NPsz1KCiKxw', 3, 'Minigun vs Lilbow G3 - The Safety Net'),
(36, 9, 'PmypOEf5_FU', 5, 'Minigun vs Lilbow G5 - The Safety Net'),
(37, 9, 'UXQ9XtAE_J8', 4, '(PART 2) Minigun vs Lilbow G4 - The Safety Net'),
(38, 9, 'lsLmpk4XE9o', 4, '(PART 1) Minigun vs Lilbow G4 - The Safety Net'),
(39, 9, 'jtHl0je0QzM', 1, 'Minigun vs Lilbow G1 - The Safety Net'),
(40, 9, '00qhAqIQCQ0', 2, 'Minigun vs Lilbow G2 - The Safety Net'),
(41, 10, 'TpcgyQNVXR4', 5, 'Krass vs State G5 - The Safety Net'),
(42, 10, '5r11aU3elEA', 3, 'Krass vs State G3 - The Safety Net'),
(43, 10, 'gwgfG6dvRUU', 2, 'Krass vs State G2 - The Safety Net'),
(44, 11, 'Mn4dfaV8dmo', 2, 'Tyga vs Catz G2 - The Safety Net'),
(45, 11, '6FfRrU5Areg', 3, 'Tyga vs Catz G3 - The Safety Net'),
(46, 11, 'QJCvRyhLSOQ', 1, 'Tyga vs Catz G1 - The Safety Net'),
(47, 12, 'j6W3rkV4mdw', 2, 'Patience/True vs Ryung/Impact G2 - Two vs Twournament'),
(48, 14, 'sA60475gGus', 3, 'Crank/Heart vs Patience/True G3 - Two vs Twournament'),
(49, 14, 'FIJcU8YeuZk', 1, 'Crank/Heart vs Patience/True G1 - Two vs Twournament'),
(50, 14, 'KHW6n8Ii6KA', 2, 'Crank/Heart vs Patience/True G2 - Two vs Twournament'),
(51, 13, 'VE_DpKa3xB8', 1, 'Crank/Heart vs Ryung/Impact G1 - Two vs Twournament'),
(52, 13, 'inBco-sL4mA', 2, 'Crank/Heart vs Ryung/Impact G2 - Two vs Twournament'),
(53, 15, 'trJZWv6jgC4', 2, 'Ryung/Impact vs SuperNova/Symbol G2 - Two vs Twournament'),
(54, 16, 'PGJglqXVkf8', 1, 'SuperNova/Symbol vs Crank/Heart G1 - Two vs Twournament'),
(55, 16, 'xngDluxVBbs', 2, 'SuperNova/Symbol vs Crank/Heart G2 - Two vs Twournament'),
(56, 17, 'Y3IznIAqDEA', 3, 'Solar/Stork vs Patience/True G3 - Two vs Twournament'),
(57, 17, 'ghSTqsWGCgY', 2, 'Solar/Stork vs Patience/True G2 - Two vs Twournament'),
(58, 17, 'CERm_UiYH5w', 1, 'Solar/Stork vs Patience/True G1 - Two vs Twournament'),
(59, 10, 'rgBk2sGOce4', 4, 'Krass vs State G4 - The Safety Net'),
(60, 10, 'XvMbZ12g-XM', 1, 'Krass vs State G1 - The Safety Net'),
(61, 24, 'wvwDAkqxtZU', 0, 'Axiom LOTV Tournament - Grand Final'),
(62, 23, 'InSf8TonAzk', 0, 'Axiom LOTV Tournament - Semifinal 2'),
(63, 22, '0Z8jqqi_LXw', 0, 'Axiom LOTV Tournament - Semifinal 1'),
(64, 21, 'F7TOQV2RCQY', 0, 'Axiom LOTV Tournament - RO8 - Heart vs. TheSTC'),
(65, 20, 'mlUOgwXOOmY', 0, 'Axiom LOTV Tournament - RO8 - CranK vs. Ryung'),
(66, 19, 'dNIN9HwH-Z4', 0, 'Axiom LOTV Tournament - RO8 - Impact vs. MMA'),
(67, 18, 'TlushgukOtU', 0, 'Axiom LOTV Tournament - RO8 - Miya vs. Alicia'),
(68, 44, '8cLuol8LTQw', 0, 'Hearthstone - Max vs. Savjz - ESL Legendary Series Finals - Quarterfinal'),
(69, 43, '8WwBRQpGBwA', 0, 'Hearthstone - Zalae vs. Chakki - ESL Legendary Series Finals - Semifinal'),
(70, 42, 'bwjvcF_aIus', 0, 'Hearthstone - Pinpingho vs. Chakki - ESL Legendary Series Finals - Quarterfinal'),
(71, 41, 'M93Vs1DTsfI', 0, 'Hearthstone - SilentStorm vs. Savjz - ESL Legendary Series Finals - Quarterfinal'),
(72, 38, 'i8hoIiNdTnY', 0, 'Hearthstone - Darkwonyx vs. SilentStorm - ESL Legendary Series Finals - Semifinal'),
(73, 37, 'w0Ksj5X6kG0', 0, 'Hearthstone - Weifu vs. Chakki - ESL Legendary Series Finals - Quarterfinal'),
(74, 36, 'TRqPWPDiexM', 0, 'Hearthstone - Chakki vs. SilentStorm - ESL Legendary Series Finals - Grand Final'),
(75, 35, '93WG1sdQJBQ', 0, 'Hearthstone - Darkwonyx vs. PinpingHo - ESL Legendary Series Finals - Groupstage'),
(76, 34, 'r1yJ_KqYenA', 0, 'Hearthstone - PinpingHo vs. Chakki - ESL Legendary Series Finals - Groupstage'),
(77, 32, 'Y7bLwBd9qwY', 0, 'Hearthstone - Chakki vs. Weifu - ESL Legendary Series Finals - Groupstage'),
(78, 31, 'P34RUg9lrSY', 0, 'Hearthstone - Zalae vs. Chakki - ESL Legendary Series Finals - Playoffs'),
(79, 30, '21oe9QvtCPY', 0, 'Hearthstone - Savjz vs. Max - ESL Legendary Series Finals - Groupstage'),
(80, 29, '_7kXfg5ZhFY', 0, 'Hearthstone - Chakki vs. Darkwonyx - ESL Legendary Series Finals - Groupstage'),
(81, 28, '8Ir4116kxKY', 0, 'Hearthstone - Zalae vs. Max - ESL Legendary Series Finals - Groupstage'),
(82, 27, '5u4Uke5SmpM', 0, 'Hearthstone - SilentStorm vs. Savjz - ESL Legendary Series Finals - Groupstage'),
(83, 26, 'Z1kc3Qq5iH8', 0, 'Hearthstone - Zalae vs. SilentStorm - ESL Legendary Series Finals - Groupstage'),
(84, 25, 'lmfl3QhkGp0', 0, 'Hearthstone - PinpingHo vs. Weifu - ESL Legendary Series Finals - Groupstage'),
(116, 25, '243-1005', 1, 'Hearthstone - PinpingHo vs. Weifu - ESL Legendary Series Finals - Groupstage'),
(117, 25, '1116-1705', 2, 'Hearthstone - PinpingHo vs. Weifu - ESL Legendary Series Finals - Groupstage'),
(118, 25, '1825-2173', 3, 'Hearthstone - PinpingHo vs. Weifu - ESL Legendary Series Finals - Groupstage'),
(119, 25, '2338-2830', 4, 'Hearthstone - PinpingHo vs. Weifu - ESL Legendary Series Finals - Groupstage');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `vodchannels_events_look_up`
--
ALTER TABLE `vodchannels_events_look_up`
  ADD CONSTRAINT `vodchannels_events_look_up_ibfk_2` FOREIGN KEY (`events_id`) REFERENCES `vodevents` (`events_id`),
  ADD CONSTRAINT `vodchannels_events_look_up_ibfk_3` FOREIGN KEY (`channels_id`) REFERENCES `vodchannels` (`channels_id`);

--
-- Constraints for table `vodevents_games_look_up`
--
ALTER TABLE `vodevents_games_look_up`
  ADD CONSTRAINT `vodevents_games_look_up_ibfk_1` FOREIGN KEY (`evenets_id`) REFERENCES `vodevents` (`events_id`),
  ADD CONSTRAINT `vodevents_games_look_up_ibfk_2` FOREIGN KEY (`games_id`) REFERENCES `vodgames` (`games_id`);

--
-- Constraints for table `vodlogin_attempts`
--
ALTER TABLE `vodlogin_attempts`
  ADD CONSTRAINT `vodlogin_attempts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `vodusers` (`users_id`);

--
-- Constraints for table `vodplayers_series_look_up`
--
ALTER TABLE `vodplayers_series_look_up`
  ADD CONSTRAINT `vodplayers_series_look_up_ibfk_1` FOREIGN KEY (`players_id`) REFERENCES `vodplayers` (`players_id`),
  ADD CONSTRAINT `vodplayers_series_look_up_ibfk_2` FOREIGN KEY (`series_id`) REFERENCES `vodseries` (`series_id`);

--
-- Constraints for table `vodseries`
--
ALTER TABLE `vodseries`
  ADD CONSTRAINT `vodseries_ibfk_1` FOREIGN KEY (`games_id`) REFERENCES `vodgames` (`games_id`),
  ADD CONSTRAINT `vodseries_ibfk_2` FOREIGN KEY (`events_id`) REFERENCES `vodevents` (`events_id`),
  ADD CONSTRAINT `vodseries_ibfk_3` FOREIGN KEY (`channels_id`) REFERENCES `vodchannels` (`channels_id`);

--
-- Constraints for table `vodvideos`
--
ALTER TABLE `vodvideos`
  ADD CONSTRAINT `vodvideos_ibfk_1` FOREIGN KEY (`series_id`) REFERENCES `vodseries` (`series_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
